
# Import the os module, for the os.walk function
import os
import sys
 
# Set the directory you want to start from
rootDir = '.'

def main( szDirPath ):
	DirectoryTraverse( szDirPath, None )

def DirectoryTraverse( szDir, fnCallback ):
	print( '[INFO]' )
	print( '[INFO] Directory Path : %s' % szDir )
	print( '[INFO]' )
	for dirName, subdirList, fileList in os.walk( szDir ):
		#print( 'Found directory: %s' % dirName )
		for fname in fileList:
			if not fname.endswith( '.txt' ):
				continue
			szFilePath = os.path.abspath( os.path.join( dirName, fname ) )
			print( '[INFO] Found txt file. %s' % szFilePath )
			print( '[INFO] >> Remove?[Y/N] ', end='', flush=True )

			for line in sys.stdin:
				line = line.strip().lower()
				#print( '[%s]' % line )
				if line in [ 'y', 'n' ]:
					break

			if line == 'y':
				print( '[INFO] >> Removed (%s)', szFilePath )
				os.remove( szFilePath )
			print( '[INFO]' )

if __name__ == '__main__':
	main( sys.argv[1] )

