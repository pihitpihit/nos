#-*- coding: utf-8 -*-

import sys
import os

sys.path.insert( 0, os.path.join( os.path.dirname( os.path.realpath(__file__) ), 'telegram-bot' ) )

from TbEightanium import EightaniumBot

szStorageBase = "D:\\Storage\\"

def Notify( szStorage, szMsg ):
	bot = EightaniumBot( True )
	szUserRoot = szStorage[ len( szStorageBase ): ].split( '\\' )[0]
	bot.SendForDownload( szUserRoot, szMsg )
	return

def NotifySimple( szMsg ):
	bot = EightaniumBot( True )
	bot.SendAdmin( szMsg )
	return

if __name__ == "__main__":
	NotifySimple( sys.argv[1] )
