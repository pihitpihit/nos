#-*- coding: utf-8 -*-

import sys, os
import random
from TbCommandBase import CommandBase


class TbCommandLotto(CommandBase):
	def __init__( self, dictSpec ):
		CommandBase.__init__( self, dictSpec )
		return

	def Run( self, message, basetext ):
		if self.RunDefault( message ):
			return

		elif self.argc == 0:
			list = []
			for i in range( 6 ):
				while True:
					tmp = random.randrange( 1, 45 )
					if tmp not in list:
						list.append( tmp )
						break
			list.sort()
			self.bot.SendMessage( message.chat.id, u'%s' % list )

		elif 1 <= self.argc:
			arg = '.'.join( self.argv )
			arg = arg.replace( u'....', u'.' )
			arg = arg.replace( u'...', u'.' )
			arg = arg.replace( u'..', u'.' )

			listRaw = [ tmp.strip() for tmp in arg.split( u'.' ) ]
			nManualCount = len( listRaw )
			
			#개수 확인
			if 6 <= nManualCount:
				self.bot.SendMessage( message.chat.id, u'[오류] 5개보다 많은 번호를 지정할 수 없습니다.' )
				return

			# 숫자 변환
			listManual = []
			try:
				for tmp in listRaw:
					ntmp = int( tmp )
					# 범위 검사
					if ntmp < 1 or 45 < ntmp:
						self.bot.SendMessage( message.chat.id, u'[오류] 1부터 45까지만 입력할 수 있습니다.' )
						return

					# 중복 검사
					if ntmp in listManual:
						self.bot.SendMessage( message.chat.id, u'[오류] 중복된 번호를 입력할 수 없습니다.' )
						return

					listManual.append( ntmp )
			except:
				self.bot.SendMessage( message.chat.id, u'[오류] 숫자,공백,점만 입력할 수 있습니다.' )
				return

			listResult = []
			listResult.extend( listManual )
			listAuto = []

			for i in range( 6 - nManualCount ):
				while True:
					tmp = random.randrange( 1, 45 )
					if tmp not in listResult:
						listResult.append( tmp )
						listAuto.append( tmp )
						break

			listManual.sort()
			listAuto.sort()
			listResult.sort()

			listRetryButton = [ [ self.InlineButton( u'다시', u'/lotto_%s' % ( arg ) ) ] ]
			listRetryMenu = self.InlineLayout( listRetryButton )
			msg = u'수동: %s\r\n자동: %s\r\n결과: %s' % ( listManual, listAuto, listResult )

			if basetext:
				self.bot.EditMessage( message, msg, listRetryMenu )
			else:
				self.bot.SendMessage( message.chat.id, msg, listRetryMenu )

		else:
			self.SendHelp( message )

		return


if __name__ == '__main__':
	print( 'test' )

