#-*- coding: utf-8 -*-

import sys, os
import random
from TbCommandBase import CommandBase


class TbCommandDice(CommandBase):
	def __init__( self, dictSpec ):
		CommandBase.__init__( self, dictSpec )
		return

	def Run( self, message, basetext ):
		if self.RunDefault( message ):
			return

		elif self.argc == 0:
			self.bot.SendMessage( message.chat.id, u'%d' % random.randrange( 1, 7 ) )

		elif 1 <= self.argc:
			arg = '.'.join( self.argv )
			arg = arg.replace( u'....', u'.' )
			arg = arg.replace( u'...', u'.' )
			arg = arg.replace( u'..', u'.' )
			listRaw = [ tmp.strip() for tmp in arg.split( u'.' ) ]
			try:
				listDice = [ int( tmp ) for tmp in listRaw ]
			except:
				self.bot.SendMessage( message.chat.id, u'[오류] 숫자,공백,점만 입력할 수 있습니다.' )
				return
			listWidth = [ len( tmp ) for tmp in listRaw ]
			listResult = []
			
			nSum = 0
			strConfig = u''
			strResult = u''
			for i in range( len( listDice ) ):
				max = listDice[i]
				result = random.randrange( 1, max + 1 )
				if i != 0:
					strResult += u','
					strConfig += u','
				strResult += str(result).rjust( listWidth[i], ' ' )
				strConfig += listRaw[i]
				nSum += result
			
			listRetryButton = [ [ self.InlineButton( u'\U0001F3B2 다시 \U0001F3B2', u'/dice_%s' % ( arg ) ) ] ]
			listRetryMenu = self.InlineLayout( listRetryButton )
			msg = u'설정: <code>%s</code>\r\n결과: <code>%s</code>\r\n합계: <b>%d</b>' % ( strConfig, strResult, nSum )

			if basetext:
				self.bot.EditMessage( message, msg, listRetryMenu )
			else:
				self.bot.SendMessage( message.chat.id, msg, listRetryMenu )

		else:
			self.SendHelp( message )

		return


if __name__ == '__main__':
	print( 'test' )

