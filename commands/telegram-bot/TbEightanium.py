#-*- coding: utf-8 -*-

import TbBase
import logging
import importlib
from TbUser import User
from TbCommandBase import CommandType
from TbCommandBase import CommandBase
from TbCommandManager import CommandManager

g_szNote  = u'<b>[EightaniumBot v1.0.0.1]</b>\r\n'
g_szNote += u'<b>[2019-0311-23:50][v1.1.0.0]</b>\r\n'
g_szNote += u'\U0001f4ac 주사위 굴리기 추가(/dice)\r\n'
g_szNote += u'\U0001f4ac 로또머신 추가(/lotto)\r\n'
g_szNote += u'<b>[2019-0310-23:19][v1.0.0.2]</b>\r\n'
g_szNote += u'\U0001f4ac /torr_listall 에서 등록한 사용자 이름이 출력되도록 개선\r\n'
g_szNote += u'\U0001f4ac 토렌트 다운로드 완료시 알림이 오지 않는 문제 개선\r\n'
g_szNote += u'<b>[2019-0310-15:10][v1.0.0.1]</b>\r\n'
g_szNote += u'\U0001f4ac /torr_list, /torr_listall 명령이 작동하지 않는 문제 개선\r\n'
g_szNote += u'\U0001f4ac /note 명령 추가(릴리즈노트 보기)\r\n'
g_szNote += u'<b>[알려진 문제]</b>\r\n'
g_szNote += u'\U0001f4ac 탐색기 사용시 telegram 이 인식하지 못하는 문자가 파일/폴더 이름에 있을 경우 응답이 없는 문제가 있음'

class EightaniumBot( TbBase.TelegramBot ):
	def __init__( self, bNotifier = False ):
		self.user = User()
		self.admin = self.user.GetAdmin()

		self.dictSpec = {}

		# Notifier 모드로 동작할 때는 명령어를 구성하지 않는다.
		if bNotifier:
			TbBase.TelegramBot.__init__( self,
				self.admin[u'token'],
				self.admin[u'chatid'],
				u'Eightanium-Notifier',
				True
				)
		else:
			TbBase.TelegramBot.__init__( self,
				self.admin[u'token'],
				self.admin[u'chatid'],
				u'Eightanium'
				)

			self.AddCommand({
				'opcode': u'/restart',
				'argc' 	: [0],
				'type' 	: CommandType.Function,
				'func' 	: self.CmdRestart,
				'help' 	: u'서비스를 재시작합니다.',
				'auth' 	: [u'admin'],
				'cobj'	: None,
			})
			self.AddCommand({
				'opcode': u'/terminate',
				'argc' 	: [0],
				'type' 	: CommandType.Function,
				'func'	: self.CmdTerminate,
				'help'	: u'서비스를 종료합니다.',
				'auth'	: [u'admin'],
				'cobj'	: None,
			})
			self.AddCommand({
				'opcode': u'/help',
				'argc' 	: [0,1],
				'type' 	: CommandType.Function,
				'func' 	: self.CmdHelp,
				'help' 	: u'사용가능한 명령어 목록을 출력합니다.',
				'auth' 	: [u'admin', u'user', u'parker'],
				'cobj'	: None,
			})
			self.AddCommand({
				'opcode': u'/bfcommand',
				'argc' 	: [0],
				'type' 	: CommandType.Function,
				'func' 	: self.CmdBfCommand,
				'help' 	: u'BotFather에게 보낼 명령어 집합을 출력합니다.',
				'auth' 	: [u'admin'],
				'cobj'	: None,
			})
			self.AddCommand({
				'opcode': u'/whoami',
				'argc' 	: [0],
				'type' 	: CommandType.Function,
				'func'	: self.CmdWhoAmI,
				'help'	: u'현재 사용자 정보를 출력합니다.',
				'auth'	: [u'admin', u'user'],
				'cobj'	: None,
			})
			self.AddCommand({
				'opcode': u'/note',
				'argc' 	: [0],
				'type' 	: CommandType.Function,
				'func'	: self.CmdNote,
				'help'	: u'릴리즈 노트를 확인합니다.',
				'auth'	: [u'admin', u'user'],
				'cobj'	: None,
			})
			self.AddCommand({
				'opcode': u'/parker',
				'argc' 	: [1],
				'type' 	: CommandType.Class,
				'cname'	: 'TbCommandParker',
				'help'	: u'주차정산 서비스를 이용합니다.',
				'auth'	: [u'admin', u'parker'],
				'cobj'	: None,
				#'start'	: True,
			})
			self.AddCommand({
				'opcode': u'/test',
				'argc' 	: [1],
				'type' 	: CommandType.Class,
				'cname'	: 'TbCommandTest',
				'help'	: u'테스트.',
				'auth'	: [u'admin'],
				'cobj'	: None,
			})
			self.AddCommand({
				'opcode': u'magnet',
				'argc' 	: [0],
				'type' 	: CommandType.ClassEmbedded,
				'cname'	: 'TbCommandMagnet',
				'help'	: u'마그넷을 등록합니다.',
				'auth'	: [u'admin', u'user'],
				'cobj'	: None,
			})
			self.AddCommand({
				'opcode': u'https://www.youtube.com/',
				'argc' 	: [0],
				'type' 	: CommandType.ClassEmbedded,
				'cname'	: 'TbCommandYoutube',
				'help'	: u'Youtube 링크를 입력하여 MP3를 다운로드 합니다.',
				'auth'	: [u'admin', u'user'],
				'cobj'	: None,
			})
			self.AddCommand({
				'opcode': u'https://youtu.be/',
				'argc' 	: [0],
				'type' 	: CommandType.ClassEmbedded,
				'cname'	: 'TbCommandYoutube',
				'help'	: u'Youtube 링크를 입력하여 MP3를 다운로드 합니다.',
				'auth'	: [u'admin', u'user'],
				'cobj'	: None,
			})
			self.AddCommand({
				'opcode': u'https://music.youtube.com/',
				'argc' 	: [0],
				'type' 	: CommandType.ClassEmbedded,
				'cname'	: 'TbCommandYoutube',
				'help'	: u'Youtube Music 링크를 입력하여 MP3를 다운로드 합니다.',
				'auth'	: [u'admin', u'user'],
				'cobj'	: None,
			})
			self.AddCommand({
				'opcode': u'/torr',
				'argc' 	: [0],
				'type' 	: CommandType.Class,
				'cname'	: 'TbCommandTorrent',
				'help'	: u'토렌트 서버를 조회합니다.',
				'auth'	: [u'admin', u'user'],
				'cobj'	: None,
			})
			self.AddCommand({
				'opcode': u'/dir',
				'argc' 	: [0],
				'type' 	: CommandType.Class,
				'cname'	: 'TbCommandDir',
				'help'	: u'탐색기를 사용합니다. (BETA)',
				'auth'	: [u'admin', u'user'],
				'cobj'	: None,
			})
			self.AddCommand({
				'opcode': u'/dice',
				'argc' 	: [0],
				'type' 	: CommandType.Class,
				'cname'	: 'TbCommandDice',
				'help'	: u'주사위를 굴립니다. (BETA)',
				'auth'	: [u'admin', u'user'],
				'cobj'	: None,
			})
			self.AddCommand({
				'opcode': u'/lotto',
				'argc' 	: [0],
				'type' 	: CommandType.Class,
				'cname'	: 'TbCommandLotto',
				'help'	: u'로또 번호를 생성합니다. (BETA)',
				'auth'	: [u'admin', u'user'],
				'cobj'	: None,
			})
			self.AddCommand({
				'opcode': u'/mxp',
				'argc' 	: [1],
				'type' 	: CommandType.Class,
				'cname'	: 'TbCommandMusicExplorer',
				'help'	: u'엘범생성기를 사용합니다.',
				'auth'	: [u'admin'],
				'cobj'	: None,
			})
			self.AddCommand({
				'opcode': u'/mat',
				'argc' 	: [1],
				'type' 	: CommandType.Class,
				'cname'	: 'TbCommandMusicArtist',
				'help'	: u'아티스트를 등록합니다.',
				'auth'	: [u'admin'],
				'cobj'	: None,
			})

			self.SendNotify( u'서비스를 초기화 했습니다.' )

		return


	def OnStart( self ):
		# 메인스레드 종료 후 발생하는 콜백
		for opcode in self.dictSpec:
			# start 속성이 True 인 경우만 서비스를 시작한다.
			if 'start' in self.dictSpec[opcode]:
				command = CommandManager.CreateCommand( self.dictSpec[opcode]['cname'], self.dictSpec[opcode] )
				self.dictSpec[opcode]['cobj'] = command
				command.OnStart()

	def OnFinish( self ):
		# 메인스레드 종료 후 발생하는 콜백
		for opcode in self.dictSpec:
			command = self.dictSpec[opcode]['cobj']
			if command:
				command.OnStop()

	def SendForDownload( self, szStorage, text ):
		return self.SendToStorage( szStorage, u'<b>[다운로드 완료]</b>\r\n' + text + self.GetRecMsgForTorrent() )

	def	SendToStorage( self, szStorage, text ):
		chatid = self.user.GetStorageOwner( szStorage )
		if chatid:
			self.SendMessage( chatid, text )
			if not self.user.IsAdmin( chatid ):
				self.SendAdmin( ( u'[통합전송-%s]\r\n' % szStorage ) + text )
		else:
			self.SendAdmin( ( u'[대체전송-%s]\r\n' % szStorage ) + text )
		return

	def GetRecMsgForTorrent( self ):
		msg  = u''
		msg += u'\r\n--------'
		msg += u'\r\n내 대기열 확인: /torr_list'
		msg += u'\r\n전체 대기열 확인: /torr_listall'
		return msg

	def AddCommand( self, dictCmdSpec ):
		opcode = dictCmdSpec['opcode']
		if opcode in self.dictSpec:
			self.SendNotify( u'Duplicated Opcode (%s)' % opcode )
		else:
			self.dictSpec[opcode] = dictCmdSpec
			if CommandType.IsClass( dictCmdSpec['type'] ):
				dictCmdSpec['bot'] = self
				# 초기화 속도 향상을 위해 Obj는 최초 실행시 만든다.
				dictCmdSpec['cobj'] = None
		return

	def RunCommand( self, dictCmdSpec, message, basetext ):
		result = None
		if self.user.GetAuth( message.chat.id ) in dictCmdSpec['auth']:

			if CommandType.IsFunction( dictCmdSpec['type'] ):
				result = dictCmdSpec['func']( message )

			elif CommandType.IsClass( dictCmdSpec['type'] ):
				cobj = dictCmdSpec['cobj']
				
				# object가 없으면 로드한다.
				if cobj == None:
					cobj = CommandManager.CreateCommand( dictCmdSpec['cname'], dictCmdSpec )
					dictCmdSpec['cobj'] = cobj

				# 이미 object 를 로드한 상태라면 변경사항이 있을 경우 다시 로드한다.
				elif CommandManager.IsModified( dictCmdSpec['cname'], dictCmdSpec ):
					self.Log( 'Reload ' + dictCmdSpec['cname'] )
					# 중지
					cobj.OnStop()

					# 레퍼런스 제거
					cobj = None
					dictCmdSpec['cobj'] = None

					# 로드
					cobj = CommandManager.CreateCommand( dictCmdSpec['cname'], dictCmdSpec, True )
					dictCmdSpec['cobj'] = cobj;
					
				result = cobj.Run( message, basetext )

			else:
				self.SendNotify( u'[InvalidCmdType]\r\n' + message.text )

		else:
			self.SendMessage( message.chat.id, u'[Permission Denied]\r\n명령을 수행할 권한이 없습니다.\r\n관리자에게 문의하세요.' )
			self.SendAdmin( u'[NoAuth-(%s)]\r\n%s' % ( self.user.GetName( message.chat.id ), message.text ) )

		if result != None:
			return result
		return True

	def GetCommandSpec( self, opcode ):
		try:
			return self.dictSpec[opcode]
		except:
			return None

	def GetEmbeddedCommandSpec( self, msg ):
		targetcmd = None

		for opcode in self.dictSpec:
			cmd = self.dictSpec[opcode]

			# opcode 포함형 명령이 아닌것은 무시
			if not CommandType.IsClass( cmd['type'] ):
				continue

			# 메시지의 시작과 opcode가 일치하는지 확인
			if msg.startswith( opcode ):
				targetcmd = cmd

		return targetcmd

	def CmdRestart( self, message ):
		return False

	def CmdTerminate( self, message ):
		self.bTerminate = True
		return False

	def CmdWhoAmI( self, message ):
		chat = message.chat

		msg = u'[UserInfo]\r\n'

		if chat.username:
			msg += u'Name: %s\r\n' % ( chat.username )
		else:
			msg += u'Name: %s %s\r\n' % ( chat.first_name, chat.last_name )

		msg += u'ChatId: %d\r\n' % ( chat.id )
		msg += u'Auth: %s\r\n' % ( self.user.GetAuth( chat.id ) )

		self.SendMessage( chat.id, msg )
		return

	def CmdBfCommand( self, message ):
		msg = u''
		for opcode in self.dictSpec:
			if not CommandType.IsEmbedded( self.dictSpec[opcode]['type'] ):
				if 'user' in self.dictSpec[opcode]['auth']:
					msg += u'%s - %s\r\n' % ( opcode[1:], self.dictSpec[opcode]['help'] )
		self.SendAdmin( msg )
		return

	def CmdHelp( self, message ):
		# admin 은 특정 권한이 사용할 수 있는 명령어 목록을 볼 수 있다.
		argv = CommandBase.GetArgv( message )
		if self.user.IsAdmin( message.chat.id ) and 1 < len( argv ):
			auth = argv[1]
		else:
			auth = self.user.GetAuth( message.chat.id )

		msg = u''
		for opcode in self.dictSpec:
			if auth in self.dictSpec[opcode]['auth']:
				msg += u'%s - %s\r\n' % ( opcode, self.dictSpec[opcode]['help'] )
		self.SendMessage( message.chat.id, msg )
		return

	def CmdNote( self, message ):
		self.SendMessage( message.chat.id, g_szNote )
		return

	def MessageHandler( self, message, basetext ):
		self.Log( 'Message:' )
		self.Log( message )
		cmd = None
		
		# 비 텍스트 기반 메시지는 여기서 처리
		if message.text == None:
			self.SendAdmin( u'[UnknownCmd]\r\n지원되지 않는 형식입니다' )
			if not self.user.IsAdmin( message.chat.id ):
				self.SendMessage( message.chat.id, u'[오류] 지원되지 않는 형식입니다.' )
			return True

		# parker는 고정 명령어를 사용한다.
		if self.user.GetAuth( message.chat.id ) == 'parker':
			message.text = '/parker_check %s' % message.text.replace( ' ', '' ).replace( '\t', '' )

		# 독립 opcode 기반 명령어는 여기서 처리 가능
		if not cmd:
			cmd = self.GetCommandSpec( CommandBase.GetArgv( message )[0] )

		# opcode 포함 명령어는 여기서 처리
		if not cmd:
			cmd = self.GetEmbeddedCommandSpec( message.text )

		# 매칭된 명령어 실행
		if cmd:
			return self.RunCommand( cmd, message, basetext )

		# 매칭된 것이 없으면 안내문구 발송
		self.SendAdmin( u'[UnknownCmd]\r\n' + message.text )
		if not self.user.IsAdmin( message.chat.id ):
			self.SendMessage( message.chat.id, u'[정의되지 않은 명령어]\r\n' + message.text )

		return True

	def CallbackHandler( self, callback ):
		self.Log( 'Callback:' )
		self.Log( callback )
		
		message = callback.message

		# 진짜 콜백처리
		if callback.data.startswith( u'cb_' ):

			# 범용 콜백 ( 취소 ) - 문구에 취소됨을 붙이고 버튼을 제거한다.
			if callback.data == u'cb_cancel':
				msg = message.text + u'\r\n(취소됨)'
				self.hCore.editMessageText( chat_id=message.chat.id, message_id=message.message_id, text=msg )

			# 기타 미구현
			else:
				self.SendMessage( callback.message.chat.id, u'콜백 실험' )
				self.hCore.editMessageText( chat_id=message.chat.id, message_id=message.message_id, text=u'바뀐텍스트' )

		# 버튼으로 제공했지만 일반 명령어에 해당
		else:
			basetext = message.text
			message.text = callback.data
			self.MessageHandler( message, basetext )

		return True


if __name__ == "__main__":
	bot = None
	while True:
		if bot:
			del bot
		print( '---------------------------------------(0-Enter)' )
		bot = EightaniumBot()
		print( '---------------------------------------(1-Created)' )
		bot.Start()
		print( '---------------------------------------(2-Leave)' )
		if bot.bTerminate:
			break
	del bot
	print( '---------------------------------------(3-Terminated)' )

