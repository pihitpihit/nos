#-*- coding: utf-8 -*-

import os
import threading

szStorageBase = os.path.join( u'D:\\', u'Storage' )

class User:
	def __init__( self ):
		self.nAdminId = 52506654

		self.dictUser = {
			self.nAdminId:{
				'name'		: u'Hyoik Lee',
				'kname'		: u'이효익',
				'account'	: u'pihipt',
				'storage'	: u'PIHIT',
				'auth'		: u'admin',
				'chatid'	: self.nAdminId,
				'token'		: u'473687016:AAE0yN2SW_GYURI8XouODxftiYpSYk3pz9w'
			},
			441685522:{
				'name'		: u'민준 김',
				'kname'		: u'김민준',
				'account'	: u'mowm',
				'storage'	: u'MOWM',
				'auth'		: u'user',
				'chatid'	: 441685522,
			},
			0:{
				'name'		: u'효범 이',
				'kname'		: u'이효범',
				'account'	: u'bigihb',
				'storage'	: u'BIGIHB',
				'auth'		: u'user',
				'chatid'	: 0,
				'car'		: u'04노4662',
			},
			1:{
				'name'		: u'TEMPO',
				'kname'		: u'TEMPO',
				'account'	: u'tempo',
				'storage'	: u'TEMPO2',
				'auth'		: u'user',
				'chatid'	: 0,
				'car'		: u'66고2957',
			},
			717593009:{
				'name'		: u'형기 김',
				'kname'		: u'김형기',
				'account'	: u'different8',
				'storage'	: u'HG',
				'auth'		: u'user',
				'chatid'	: 717593009,
				'car'		: u'42버6246',
			},
			815157969:{
				'name'		: u'혜진 정',
				'kname'		: u'정혜진',
				'account'	: u'haseda',
				'auth'		: u'user',
				'chatid'	: 815157969,
			},
			373871391:{
				'name'		: u'홍임 최',
				'kname'		: u'최홍임',
				'account'	: u'HONG',
				'auth'		: u'parker',
				'chatid'	: 373871391,
			},
			# 1:{
			#	/* static property - 처음부터 세팅되어있는 것들 */
			#	'name'		: u'test',
			#	'kname'		: u'테스트',
			#	'account'	: u'test',
			#	'storage'	: u'test',
			#	'auth'		: u'user',
			#	'chatid'	: 1,
			#	'car'		: u'28러1111',
			#	/* runtime property - 운용하면서 생기는 것들 */
			#	'cd'		: /* current directory */
			#	'session'	: {
			#		'mxp' : ( 'hmxp, timer ) /* mxp 핸들, 세션 자동종료 타이머 */
			#	}
			# },
		}
		return

	def GetAdmin( self ):
		return self.dictUser[self.nAdminId]

	def GetUser( self, chatid ):
		return self.dictUser[chatid]

	def GetName( self, chatid ):
		try:
			return self.dictUser[chatid]['name']
		except:
			return u'(Unknown)'

	def GetAuth( self, chatid ):
		try:
			return self.GetUser( chatid )['auth']
		except:
			return None

	def IsAdmin( self, chatid ):
		return self.nAdminId == chatid

	def AdjustChatId( self, chatid ):
		if chatid == None or chatid < 100:
			return None
		return chatid

	def GetStorageOwner( self, szStorage ):
		for user in self.dictUser:
			oneuser = self.dictUser[user]
			if 'storage' in oneuser and oneuser['storage'] == szStorage:
				return self.AdjustChatId( user )
		return None	

	def PathToUserName( self, szPath ):
		szPathNoBase = szPath[len(szStorageBase)+1:]
		#print( szPathNoBase )
		for user in self.dictUser:
			oneuser = self.dictUser[user]
			if 'storage' in oneuser and szPathNoBase.startswith( oneuser['storage'] ):
				return oneuser['storage']
		return u'(Unknown)'

	def GetStorage( self, chatid ):
		try:
			return self.dictUser[chatid]['storage']
		except:
			return None

	def GetOwnerChatId( self, szCar ):
		for user in self.dictUser:
			oneuser = self.dictUser[user]
			if 'car' in oneuser and oneuser['car'] == szCar:
				return self.AdjustChatId( user )
		return None	

	def GetCarList( self, bIncludeKnown = False ):
		listCar = []
		for user in self.dictUser:
			oneuser = self.dictUser[user]
			if 'car' in oneuser:
				listCar.append( ( oneuser['car'], self.AdjustChatId( user ) ) )

		if bIncludeKnown:
			listCar.append( ( u'38모3877', None ) ) # 효창
			listCar.append( ( u'15무2454', None ) ) # 재광

		return listCar

	def EnterDir( self, chat_id, szDir ):
		try:
			user = self.dictUser[chat_id]
			szCurDir = None

			if szDir == None:
				szCurDir = os.path.join( szStorageBase, user['storage'] )
			elif 'cd' not in user:
				szCurDir = os.path.join( szStorageBase, user['storage'], szDir )
			else:
				szCurDir = os.path.join( user['cd'], szDir )

			user['cd'] = szCurDir
			return szCurDir

		except:
			return None

	def LeaveDir( self, chat_id ):
		try:
			user = self.dictUser[chat_id]
			szCurDir = self.GetCurDir( chat_id )

			if self.IsHomeDir( chat_id, szCurDir ):
				return szCurDir
			
			szCurDir = szCurDir[0 : -len( os.path.basename( szCurDir ) ) - 1]
			user['cd'] = szCurDir
			return szCurDir

		except:
			return None

	def IsHomeDir( self, chat_id, szPath = None ):
		if not szPath:
			szPath = self.GetCurDir( chat_id )
		return szPath == self.GetHomeDir( chat_id )

	def GetCurDir( self, chat_id ):
		try:
			user = self.dictUser[chat_id]

			if 'cd' not in user:
				return self.GetHomeDir( chat_id )
			else:
				return user['cd']

		except:
			return None

	def GetHomeDir( self, chat_id ):
		try:
			user = self.dictUser[chat_id]
			return os.path.join( szStorageBase, user['storage'] )

		except:
			return None

	def SetSession( self, chat_id, szSession, hSession = None, nTimeout = None ):
		try:
			user = self.dictUser[chat_id]
			if 'session' not in user:
				user['session'] = {}
			if szSession in user['session']:
				session, timeout = user['session'][szSession]
				if session: del session

			# 일반적인 호출
			if hSession is not None and nTimeout is not None:
				timer = threading.Timer( nTimeout, self.ClearSession, [chat_id, szSession] )
				timer.daemon = True
				timer.start()
			# Clear 에서 호출한 케이스
			else:
				timer = None

			user['session'][szSession] = ( hSession, timer )
			#print( 'set session success', hSession, timer )

		except:
			#print( 'set session error' )
			pass
		return

	def GetSession( self, chat_id, szSession ):
		try:
			return self.dictUser[chat_id]['session'][szSession][0]
		except:
			return None

	def ClearSession( self, chat_id, szSession ):
		self.SetSession( chat_id, szSession )
		return


