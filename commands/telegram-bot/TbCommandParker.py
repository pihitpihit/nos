#-*- coding: utf-8 -*-

import sys, os
import traceback
sys.path.insert( 0, os.path.join( '..', 'pyutil' ) )
from PuInstImport import InstImport
from PuWebBrowser import WebBrowser
requests = InstImport( 'requests' )

from TbCommandBase import CommandBase


class TbCommandParker(CommandBase):
	def __init__( self, dictSpec ):
		CommandBase.__init__( self, dictSpec )
		self.hCarList = CarList( self.bot.user.GetCarList() )
		self.hParker = Parker( self.bot, self.hCarList )
		return

	def Run( self, message, basetext ):

		self.hParker.bot.message = message

		if self.RunDefault( message ):
			return

		elif 2 < self.argc:
			self.SendHelp( message )

		elif self.argc == 1 and self.argv[0] == u'start':
			self.hParker.Start()

		elif self.argc == 1 and self.argv[0] == u'stop':
			self.hParker.Stop()

		elif self.argc == 1 and self.argv[0] == u'status':
			if self.hParker.bAlive:
				self.bot.SendMessage( message.chat.id, u'[Parker] 서비스 동작중 (Cycle:%d)' % self.hParker.nCycle )
			else:
				self.bot.SendMessage( message.chat.id, u'[Parker] 서비스 중지됨' )

		elif self.argc == 1 and self.argv[0] == u'list':
			if not self.hParker.Status():
				self.hParker.Stop()
			else:
				self.bot.SendMessage( message.chat.id, u'<b>[차량 현황]</b>' + self.hCarList.Serialize() )

		elif self.argc == 1 and self.argv[0] == u'check':
			listCarButton = []
			for car in self.bot.user.GetCarList( bIncludeKnown=True ):
				listCarButton.append( self.InlineButton( car[0], u'/parker_check %s' % car[0] ) )
			listCarButton.append( self.InlineButtonCancel() )
			listCarMenu = self.InlineMenu( listCarButton, cols=1 )
			self.bot.SendMessage( message.chat.id, u'<b>차량을 선택하세요</b>', listCarMenu )

		elif self.argc == 2 and self.argv[0] == u'check':
			szTargetCar = self.argv[1]

			if basetext:
				self.bot.EditMessage( message, u'[Parker] %s 차량을 확인합니다.' % szTargetCar )
			else:
				self.bot.SendMessage( message.chat.id, u'[Parker] %s 차량을 확인합니다.' % szTargetCar )

			self.bot.message = message

			hTempCarList = CarList( [ ( szTargetCar, None ) ] )
			hTempParker = Parker( self.bot, hTempCarList )
			hTempParker.SetOneTime( True )
			hTempParker.SetNameAlias( 'OneTimeChecker' )
			hTempParker.Start()

		else:
			self.SendHelp( message )

		
		self.bot.Log( self.argv )
		return

	def OnStart( self ):
		self.hParker.Start()
		return

	def OnStop( self ):
		self.hParker.Stop( bNotify = False )
		return

	def OnHelp( self ):
		msg = self.GetDefaultHelp() + u'\r\n'
		msg += self.MakeSubHelp( u'/parker_start'	, u'서비스를 시작합니다.'			)
		msg += self.MakeSubHelp( u'/parker_stop'	, u'서비스를 중지합니다.'			)
		msg += self.MakeSubHelp( u'/parker_list'	, u'등록된 차량 현황을 출력합니다.'	)
		msg += self.MakeSubHelp( u'/parker_status'	, u'서비스 상태를 출력합니다.'		)
		msg += self.MakeSubHelp( u'/parker_check (차량번호)', u'입력된 차량을 정산합니다.' )
		return msg


import threading
import time

class Parker:
	def __init__( self, bot, hCarList ):
		self.bot = bot
		self.hCarList = hCarList
		self.hThread = None
		self.hEvent = None
		self.bAlive = False
		self.bOneTime = False
		self.nCycle = 0

		self.szNameAlias = False

		self.szStop = u'\r\n\U000023F9서비스 중지 : /parker_stop'
		self.szStart = u'\r\n\U000025B6서비스 시작 : /parker_start'
		self.szStatus = u'\r\n\U0001F4CA서비스 상태 : /parker_status'
		self.szList = u'\r\n\U0001f697차량 현황 : /parker_list'
		
		return

	def IsStarted( self ):
		return self.hThread != none

	# OneTime Parker로 세팅되면 1회만 주차정산을 시도하고 스레드가 종료된다.
	def SetOneTime( self, bOneTime ):
		self.bOneTime = bOneTime
		return

	def SetNameAlias( self, szAlias ):
		self.szNameAlias = szAlias
		return

	def Start( self, bAsync = True ):
		# already started
		if self.hThread:
			self.bot.SendAdmin( u'[Parker] 이미 동작중입니다.' + self.szStop + self.szStatus )
			return

		# 스레드 생성
		if not self.bOneTime:
			self.bot.SendAdmin( u'[Parker] 서비스를 시작합니다.' )
		self.hEvent = threading.Event()
		self.hWaiter = threading.Event()
		self.hThread = threading.Thread( target = self.Worker )
		self.hThread.deamon = True
		self.bAlive = True
		self.nCycle = 0
		self.hThread.start()

		# 로그인 성공까지 최대 15초 대기
		self.hEvent.wait( 15 )
		self.hEvent.clear()
		if not self.bOneTime:
			self.bot.SendAdmin( u'[Parker] 서비스를 시작했습니다.' + self.szStop + self.szStatus + self.szList )
		return

	def Stop( self, bNotify = True ):
		# 스레드가 없으면 서비스가 동작중이지 않은 상태다
		if self.hThread == None:
			if bNotify: self.bot.SendAdmin( u'[Parker] 서비스가 동작하고 있지 않습니다.' + self.szStart )
			return

		# 종료 요청
		if bNotify: self.bot.SendAdmin( u'[Parker] 서비스 중지를 시도합니다.' )
		self.bAlive = False
		self.hEvent.set()
		self.hWaiter.set()

		# 종료 대기
		self.hThread.join()

		# 정리
		self.hThread = None
		self.hWaiter = None
		self.hEvent = None
		if bNotify: self.bot.SendAdmin( u'[Parker] 서비스를 중지했습니다.' + self.szStart )
		return

	def Status( self ):
		return self.bAlive

	def Worker( self ):
		bInitialized = False
		bReInitialize = True

		# 기본 설정
		while bReInitialize:

			bro = None
			bInitialized = False
			bReInitialize = False

			try:
				# 초기화 <<<<
				if self.szNameAlias:
					bro = WebBrowser( 'EightParker-' + self.szNameAlias )
				else:
					bro = WebBrowser( 'EightParker' )
				bro.Start()

				# 로그인
				bro.Goto( 'http://webhills.iptime.org' )
				bro.GetElement( 'name', 'login_id' ).send_keys( '2101202' )
				bro.GetElement( 'name', 'login_pw' ).send_keys( '4702' )
				bro.GetElement( 'xpath', '/html/body/div/div/form/center/button[1]' ).click()

				# 로그인 검증
				element = bro.GetElement( 'xpath', u'/html/body/div[2]/h2' )
				if element == None or element.text != u'주차할인':
					bro.End()
					continue

				# 로그인 성공 - 서비스를 띄우는데 성공
				self.hEvent.set()
				bInitialized = True
				self.nCycle = 0
				# 초기화 >>>>

				# 검색 및 주차정산을 시작한다.
				while self.bAlive:

					hFoundRow = None

					# 차량 검색
					self.bot.Log( '차량검색' )
					for car in self.hCarList.dictCar:

						# 새로고침
						bro.Refresh()

						# 번호입력 ( 4자리 번호로만 검색이 가능하다. )
						bro.Wait( 3 )
						bro.GetElement( 'name', 'searchCarNo' ).send_keys( car[-4:] )

						# 확인버튼 클릭
						bro.Wait( 3 )
						bro.GetElement( 'name', 'btnSearch' ).click()

						# 차량 목록 획득
						bro.Wait( 3 )
						nTryRemain = 3

						# 최대 3회 재시도
						while 0 < nTryRemain:
							try:
								rows = bro.GetElements( 'xpath', '//*[@id="divAjaxCarList"]/tr/td/a' )

								self.bot.Log( '%s : %s : %s' % ( car, car[-4:], len( rows ) ) )

								# 검색 결과에서 차량번호를 찾는다.
								if len( rows ):
									for row in rows:
										if row.text == car:
											hFoundRow = row
											break
								break
							except:
								nTryRemain -= 1
								
								# 세번째에도 망하면 Exception 발생
								if nTryRemain == 0:
									raise

								# 재시도 할 때에는 3초 대기
								self.hWaiter.wait( 3 )
								continue

						# 현재 차량이 들어와 있는 상태
						if hFoundRow:
							# 클릭해서 현재상태를 확인할 수 있도록 한다.
							hFoundRow.click()

							# 입차시간을 확인한다.
							szInTimeNew = bro.GetElement( 'name', 'InTime' ).get_attribute( 'value' )

							# 입차시간이 같으면 이미 정산 된 것이므로 무시
							szIntimeOld = self.hCarList.GetInTime( car )
							if szIntimeOld != None and szIntimeOld == szInTimeNew:
								self.bot.Log( '이미처리된 차량' )
								hFoundRow = None

						# 현재 차량이 나간 상태 ( 검색 결과에 없음 )
						else:
							if self.hCarList.GetState( car )!= u'출차':
								self.hCarList.SetOut( car )

						# 정산 해야 하는 차량이 있으면 검색 종료
						if hFoundRow:
							break


					# 검색된 차량이 있으면 정산을 시도한다.
					if hFoundRow:
						szFoundCar = hFoundRow.text
						self.bot.Log( u'%s 정산시도' % szFoundCar )

						hFoundRow.click()
						szInTime = bro.GetElement( 'name', 'InTime' ).get_attribute( 'value' )
						self.bot.Log( u'입차시간 %s ' % szInTime )

						btn = bro.GetElement( 'name', 'btnDCName' )
						self.bot.Log( btn.get_attribute( 'value' ) )
						btn.click()

						# 팝업이 뜨는데 시간이 걸릴 수 있으므로 충분히 기다린다.
						self.hWaiter.wait( 5 )

						# 전액할인 버튼을 클릭하면 팝업이 뜬다. 팝업의 내용으로 성공여부를 확인한다.
						alert = bro.GetAlert()

						# 정산 성공
						if u'할인처리 되었습니다.' == alert.text:
							self.hCarList.SetIn( szFoundCar, szInTime )
							if self.bot:
								# 관리자 알림
								self.bot.SendDual( u'%s 차량이 정산처리됨.' % szFoundCar )
								# 차량 주인 알림 ( 없으면 관리자 알림 )
								chatid = self.bot.user.GetOwnerChatId( szFoundCar )
								if chatid:
									self.bot.SendMessage( chatid, u'[알림] %s 차량이 정산처리되었습니다.' % szFoundCar )
								else:
									self.bot.SendAdmin( u'[알림] %s 미등록사용차 차량이 정산처리되었습니다.' % szFoundCar )

						# 중복 정산 ( 이 경우는 발생하지 않는다. )
						elif u'이미 할인처리 되었습니다.' == alert.text:
							self.hCarList.SetIn( szFoundCar, szInTime )
							if self.bot:
								self.bot.SendAdmin( u'%s 차량이 정산처리됨.(중복)' % szFoundCar )

						# 원하는 내용이 없으면 실패
						else:
							if self.bot:
								self.bot.SendDual( u'%s 차량 정산처리 실패.' % szFoundCar )

						# 팝업을 닫는다.
						alert.accept()

					# 1회성 동작일 경우 대기 없이 종료
					if self.bOneTime:
						if not hFoundRow:
							self.bot.SendDual( u'검색된 차량이 없습니다.' )
						self.bAlive = False
						bro.End()
						break

					# 정산을 했으면 아직 완료되지 않은 정산이 있을지 모르니 조금만 대기 (10초)
					if hFoundRow:
						self.hWaiter.wait( 10 )

					# 아니면 idle 한 상태이므로 좀 더 오래 대기
					else:
						self.nCycle += 1
						now = time.localtime()

						# 오후 11시부터 오전7시까지는 40분 대기
						if now.tm_hour < 7 or 23 <= now.tm_hour:
							self.hWaiter.wait( 60 * 40 )
						# 그 외에는 15분 대기
						else:
							self.hWaiter.wait( 60 * 15 )

			except:
				traceback.print_exc()

				if bro:
					bro.End()
					bro = None

				if bInitialized:
					self.bot.SendAdmin( u'[오류]주차정산 서비스 오류 발생.' )
				else:
					self.bot.SendAdmin( u'[오류]주차정산 서비스 초기화 오류 발생.' )

				if not self.bOneTime:
					bReInitialize = True

				continue

		return


class CarList:
	def __init__( self, listCar ):
		self.dictCar = {}
		for ( szCar, chatid ) in listCar:
			self.dictCar[szCar] = ( chatid, u'미확인', None ) # 주인, 상태, 입차시각
		return

	def IsAllowedState( self, szState ):
		return szState in [ u'입차', u'출차' ]

	def SetIn( self, szCar, intime ):
		if szCar in self.dictCar:
			chatid = self.dictCar[szCar][0]
			self.dictCar[szCar] = ( chatid, u'입차', intime )
			return True
		return False

	def SetOut( self,szCar ):
		if szCar in self.dictCar:
			chatid = self.dictCar[szCar][0]
			self.dictCar[szCar] = ( chatid, u'출차', None )
			return True
		return False

	def GetOwnerChatId( self, szCar ):
		return self.dictCar[szCar][0]

	def GetState( self, szCar ):
		return self.dictCar[szCar][1]

	def GetInTime( self, szCar ):
		return self.dictCar[szCar][2]

	def Serialize( self ):
		str = u''
		for szCar in self.dictCar:
			( chatid, state, intime ) = self.dictCar[szCar]
			str += u'\r\n'
			str += u'\U0001f697%s\r\n' % ( szCar )
			str += u'\U0001f4ac%s\r\n' % ( state )
			if intime:
				str += u'\U000023f1%s\r\n' % ( intime )
		return str



if __name__ == '__main__':
	print( 'test' )
	carlist = CarList([ ( u'04노4662', 0 ), ( u'서울42더1452', 0 ), ( u'28러1111', 0 ) ])
	print( carlist.Serialize() )
	parker = Parker( None, carlist )
	parker.Start()

