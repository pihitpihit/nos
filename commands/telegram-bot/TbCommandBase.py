#-*- coding: utf-8 -*-

import importlib
import re
from enum import Enum

from telegram import InlineKeyboardButton, InlineKeyboardMarkup

class CommandType(Enum):
	# 단순 함수 호출
	Function = 0

	# CommandBase 기반
	Class = 1

	# 단순 함수 호출 + ( 명령어 + 인자 일체형 )
	FunctionEmbedded = 2

	# CommandBase 기반 + ( 명령어 + 인자 일체형 )
	ClassEmbedded = 3

	def IsClass( type ):
		return type in [ CommandType.Class, CommandType.ClassEmbedded ]

	def IsFunction( type ):
		return type in [ CommandType.Function, CommandType.FunctionEmbedded ]

	def IsEmbedded( type ):
		return type in [ CommandType.ClassEmbedded, CommandType.FunctionEmbedded ]


class CommandBase:
	def __init__( self, dictSpec ):
		self.dictSpec = dictSpec
		self.bot = dictSpec['bot']
		return

	def Run( self, message, basetext ):
		pass

	def OnStart( self ):
		pass
	def OnStop( self ):
		pass
	def OnHelp( self ):
		pass

	def RunDefault( self, message ):
		self.argv = CommandBase.GetArgv( message )[1:]
		self.argc = len( self.argv )

		if 1 <= len( self.argv ) and self.argv[0] == u'help':
			self.SendHelp( message )
			return True

		return

	def SendHelp( self, message ):
		msg = self.OnHelp()
		if not msg:
			msg = self.GetDefaultHelp()
		self.bot.SendMessage( message.chat.id, msg )
		return

	def GetDefaultHelp( self ):
		return self.dictSpec['help']

	def MakeSubHelp( self, op, desc ):
		return u'<b>[</b>' + op  + u'<b>]</b>\r\n\t\t\t' + desc + u'\r\n'

	def GetArgv( message ):
		list = re.split( u'[=]', message.text )
		args = re.split( u'[\s_]', list[0] )
		if len( list ) == 2:
			args.append( list[1] )
		return args

	def InlineButton( self, caption, data = None ):
		if not data:
			data = 'cb_%s_%s' % ( self.dictSpec['cname'], caption )
		return InlineKeyboardButton( caption, callback_data = data )

	def InlineButtonCancel( self, caption=None ):
		if not caption:
			caption = u'취소'
		return self.InlineButton( caption, u'cb_cancel' )

	def InlineMenu( self, listInlineButton, cols=None ):
		if cols == None:
			cols = len( listInlineButton ) - 1
		return InlineKeyboardMarkup( build_menu( listInlineButton, cols ) )

	def InlineLayout( self, listInlineButton ):
		return InlineKeyboardMarkup( listInlineButton )

def build_menu(buttons, n_cols, header_buttons=None, footer_buttons=None):
	menu = [buttons[i:i + n_cols] for i in range(0, len(buttons), n_cols)]
	if header_buttons:
		menu.insert(0, header_buttons)
	if footer_buttons:
		menu.append(footer_buttons)
	return menu



