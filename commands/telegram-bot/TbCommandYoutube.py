#-*- coding: utf-8 -*-

import sys, os
from TbCommandBase import CommandBase
sys.path.insert( 0, os.path.join( '..', 'pyutil' ) )
from PuYoutubeDownloader import ydl

def OnArchive( szFilePath, args ):
	# args fetch
	bot = args[0]
	message = args[1]
	index = args[2] + 1

	# udpate message
	message = bot.AppendMessage( message, '(완료)%s' % ( szFilePath.replace( '.mp3', '' ) ), '\r\n' )

	# refresh args
	return [ bot, message, index ]

def OnProgress( szPercent, args ):
	# args fetch
	bot = args[0]
	message = args[1]

	# udpate message
	try:
		message = bot.AppendMessage( message, '(다운로드중)%s' % ( szPercent ), '\r\n' )
	except:
		# 자주 보내면 익셉션이 떨어진다. 그냥 무시
		pass
	return None

def OnDownloaded( szFilePath, args ):
	# args fetch
	bot = args[0]
	message = args[1]

	# udpate message
	message = bot.AppendMessage( message, '(변환중)%s' % ( szFilePath.replace( '.mp3', '' ) ), '\r\n' )
	return None

class TbCommandYoutube(CommandBase):
	def __init__( self, dictSpec ):
		CommandBase.__init__( self, dictSpec )
		self.ydl = ydl()
		self.ydl.SetArchiveDirectory( os.path.join( 'D:\\', 'Storage', 'STREAM', '_NotTagged' ) )
		return

	def Run( self, message, basetext ):
		if self.RunDefault( message ):
			return

		szUrl = message.text

		message = self.bot.SendMessage( message.chat.id, '[YouTube]다운로드를 시작합니다.' )

		self.ydl.SetOnArchive( OnArchive )
		self.ydl.SetOnDownloaded( OnDownloaded )
		self.ydl.SetOnProgress( OnProgress, 5 ) # 사이클에 한번씩 호출
		self.ydl.SetUserData( [ self.bot, message, 0 ] )
		self.ydl.Download( szUrl )

		# 결과 확인 - user data 에 있음
		userdata = self.ydl.GetUserData()
		message = userdata[1]
		count = userdata[2]

		self.bot.AppendMessage( message, '[YouTube]%d개의 파일이 다운로드됨\r\n[YouTube]엘범만들기:/mxp' % count, '\r\n' )
		return



if __name__ == '__main__':
	print( 'test' )

