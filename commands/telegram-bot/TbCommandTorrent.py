#-*- coding: utf-8 -*-

import sys, os
from TbCommandBase import CommandBase

sys.path.insert( 0, os.path.join( "D:\\", "Develop", "commands", "uTorrent-Remover-master" ) )
import Register


class TbCommandTorrent(CommandBase):
	def __init__( self, dictSpec ):
		CommandBase.__init__( self, dictSpec )
		return

	def Run( self, message, basetext ):
		if self.RunDefault( message ):
			return

		elif self.argc != 1:
			self.SendHelp( message )

		elif self.argv[0] == u'list':
			# 저장소 확인
			szStorage = self.bot.user.GetStorage( message.chat.id )
			if not szStorage:
				self.SendStorageError( message.chat.id )
				return
			
			msg = self.GetListMessage( szStorage )

			self.bot.SendMessage( message.chat.id, msg )

		elif self.argv[0] == u'listall':
			msg = self.GetListMessage()

			self.bot.SendMessage( message.chat.id, msg )

		else:
			self.SendHelp( message )

		return

	def OnHelp( self ):
		msg = self.GetDefaultHelp() + u'\r\n'
		msg += self.MakeSubHelp( u'/torr_list'		, u'나의 토렌트 목록을 조회합니다.'	)
		msg += self.MakeSubHelp( u'/torr_listall'	, u'모든 토렌트 목록을 조회합니다.'	)
		return msg

	def GetListMessage( self, storage = None ):
		nIndex = -1
		nCollected = 0
		msg = u''

		listDownloads = Register.Enum()
		for data in listDownloads:
			nIndex += 1

			self.bot.Log( u'%s - %s' % ( storage, data[26] ) )
			if storage and storage not in data[26]:
				continue

			# 두번째부터는 한줄 더 띄운다.
			if 0 < nCollected:
				msg += u'--------\r\n'

			msg += u'<b>[대기열-%d]</b>\r\n' % nIndex
			msg += u'이름: %s\r\n' % data[ 2] #Name

			# 크기가 1GB 를 넘으면 GB단위로 출력
			if 1024*1024*1024 <= data[ 3]:
				msg += u'크기: %.2fGB\r\n' % ( data[ 3]/(1024*1024*1024) ) #Size
			else:
				msg += u'크기: %.2fMB\r\n' % ( data[ 3]/(1024*1024) ) #Size

			# 크기가 1MB 를 넘으면 MB단위로 출력
			if 1024*1024 <= data[ 9]:
				msg += u'속도: %.2fMB\r\n' % ( data[ 9]/(1024*1024) ) #Speed
			else:
				msg += u'속도: %.2fKB\r\n' % ( data[ 9]/(1024) ) #Speed

			msg += u'완료: %s\r\n' % ( data[21][len('Downloading '):] ) #Ratio

			if not storage:
				msg += u'등록: %s\r\n' % ( self.bot.user.PathToUserName( data[26] ) ) #UserName

			nCollected += 1

		# 수집된 대기열이 없으면 없다고 알려줌
		if nCollected == 0 :
			if storage:
				msg = u'(내 대기열 없음)'
			else:
				msg = u'(대기열 없음)'

		# 추천 명령어
		msg += self.bot.GetRecMsgForTorrent()

		return msg

	def SendStorageError( self, chatid ):
		self.bot.SendMessage( chatid, u'[오류] 저장소가 할당되지 않았습니다.\r\n관리자에게 문의해주세요.' )
		return

	def GetRecommandedMsg():
		msg  = u''
		msg += u'--------'
		msg += u'\r\n내 대기열 확인: /torr_list'
		msg += u'\r\n전체 대기열 확인: /torr_listall'
		return msg


if __name__ == '__main__':
	print( 'test' )

