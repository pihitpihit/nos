#-*- coding: utf-8 -*-

import sys, os
from TbCommandBase import CommandBase

class TbCommandMusicArtist(CommandBase):
	def __init__( self, dictSpec ):
		CommandBase.__init__( self, dictSpec )
		return

	def Run( self, message, basetext ):
		if self.RunDefault( message ):
			return

		count = self.bot.user.GetSession( message.chat.id, 'test' )
		if count is None:
			count = 1

		next_count = count + 1
		show_list = []
		#show_list.append( self.InlineButton( "on" ) )
		#show_list.append( self.InlineButton( "off" ) )
		#show_list.append( self.InlineButton( "cancel" ) )
		show_list.append( self.InlineButton( u"test", u'/test' ) )
		show_markup = self.InlineMenu( show_list, cols=1 ) # make markup

		# 업데이트
		self.bot.user.SetSession( message.chat.id, 'test', next_count )

		# 메시지 보내기
		if not basetext:
			self.bot.SendMessage( message.chat.id, u'CommandTest %d' % next_count, show_markup )
		else:
			self.bot.EditMessage( message, u'CommandTest %d' % next_count, show_markup )

		return



if __name__ == '__main__':
	print( 'test' )

