#-*- coding: utf-8 -*-

import importlib
import os

class CommandManager:
	def __init__( self ):
		return

	def CreateCommand( command, spec, reload = False ):
		module = importlib.import_module( command )
		if reload:
			importlib.reload( module )
		spec['mtime'] = os.path.getmtime( command + '.py' )
		return getattr( module, command )( spec )

	def IsModified( command, spec ):
		return spec['mtime'] != os.path.getmtime( command + '.py' )
