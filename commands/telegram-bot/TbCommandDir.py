#-*- coding: utf-8 -*-

import sys, os
from TbCommandBase import CommandBase

szStorageBase = os.path.join( u'D:\\', u'Storage' )

class TbCommandDir(CommandBase):
	def __init__( self, dictSpec ):
		CommandBase.__init__( self, dictSpec )
		return

	def Run( self, message, basetext ):
		if self.RunDefault( message ):
			return

		curDir = self.bot.user.GetCurDir( message.chat.id )
		index = 0

		# Home과 같다
		if self.argc == 0:
			curDir = self.bot.user.EnterDir( message.chat.id, None )
			index = 0

		elif self.argc == 1:
			if self.argv[0] == u'leave':
				curDir = self.bot.user.LeaveDir( message.chat.id )
				index = 0
			elif self.argv[0] == u'home':
				curDir = self.bot.user.EnterDir( message.chat.id, None )
				index = 0
			elif self.argv[0] == u'exit':
				if not basetext:
					self.bot.SendMessage( message.chat.id, u'탐색기가 종료되었습니다.\r\n다시 열기 : /dir' )
				else:
					self.bot.EditMessage( message, u'탐색기가 종료되었습니다.\r\n다시 열기 : /dir' )
				return
			elif self.argv[0] == u'menu':
				self.bot.EditMessage( message, u'메뉴 테스트 중.\r\n다시 열기 : /dir' )
				return
			else:
				try:
					index = int( self.argv[0] )
				except:
					index = 0

		elif self.argc == 2:
			if self.argv[0] == u'enter':
				curDir = self.bot.user.EnterDir( message.chat.id, self.argv[1] )
				index = 0

		self.bot.Log( '[DIR] CurrentDir : %s' % curDir )
		listDir = os.listdir( curDir )
		self.bot.Log( '[DIR] %s' % listDir )
		nDirs = 0
		nFiles = 0

		for item in listDir:
			if os.path.isfile( os.path.join( curDir, item ) ):
				nFiles += 1
			else:
				nDirs += 1

		listFsButton = []
		( listItem, index ) = self.GetFromList( listDir, index, 4 )
		for item in listItem:
			if os.path.isfile( os.path.join( curDir, item ) ):
				listFsButton.append( [ self.InlineButton( u'\U0001F4C4%s' % item ) ] )
			else:
				listFsButton.append( [ self.InlineButton( u'\U0001F4C2%s' % item, u'/dir_enter=%s' % item ) ] )
		listFsButton.append( self.GetDirMenu( index ) )
		listFsMenu = self.InlineLayout( listFsButton )

		# count info
		msg  = u'[현재경로] <code>%s</code>\r\n' % ( curDir[len(szStorageBase):] )
		msg += u'[폴더개수] <code>%d</code>\r\n' % ( nDirs )
		msg += u'[파일개수] <code>%d</code>\r\n' % ( nFiles )

		# 등록
		if not basetext:
			self.bot.SendMessage( message.chat.id, msg, listFsMenu )
		else:
			self.bot.EditMessage( message, msg, listFsMenu )
		return

	def GetDirMenu( self, idx ):
		listButton = []
		listButton.append( self.InlineButton( u'\U0001F3E0', u'/dir_home' ) )
		listButton.append( self.InlineButton( u'\U0001F519', u'/dir_leave' ) )
		listButton.append( self.InlineButton( u'\U0001F53C', u'/dir_%d' % ( idx - 4 ) ) )
		listButton.append( self.InlineButton( u'\U0001F53D', u'/dir_%d' % ( idx + 4 ) ) )
		listButton.append( self.InlineButton( u'\U00002795', u'/dir_menu' ) )
		listButton.append( self.InlineButton( u'\U0000274C', u'/dir_exit' ) )
		return listButton

	def GetFromList( self, listDir, nStart, nCount ):
		nTotal = len( listDir )

		if nStart < 0:
			nStart = 0

		# 갯수가 적거나 딱 맞으면 전체 반환
		if nTotal <= nCount:
			return ( listDir, 0 )

		# 반환 영역이 끝 부분에 걸치면 뒷 부분 반환
		elif nTotal <= nStart + nCount:
			return ( listDir[-nCount:], nTotal - nCount )

		# 그 외에는 요청받은 영역 반환
		else:
			return ( listDir[nStart:nStart+nCount], nStart )

	def GetFromPath( self, szDir, nStart, nCount ):
		listDir = os.listdir( szDir )
		return self.GetFromList( listDir, nStart, nCount )



if __name__ == '__main__':
	print( 'test' )

