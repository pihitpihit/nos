#-*- coding: utf-8 -*-

import sys, os
from TbCommandBase import CommandBase
sys.path.insert( 0, os.path.join( '..', 'pyutil' ) )
from Mxplorer import Mxplorer

def OnCommit( szType, hData, hUserData ):
	bot = hUserData
	if szType == 'start':
		bot.message = bot.AppendMessage( bot.message, u'앨범 마스터링 시작', u'\r\n' )
	elif szType == 'cover':
		bot.message = bot.AppendMessage( bot.message, u'커버 이미지 생성중', u'\r\n' )
	elif szType == 'tag':
		bot.message = bot.AppendMessage( bot.message, u'메타데이터 태깅중', u'\r\n' )
	elif szType == 'upload':
		bot.message = bot.AppendMessage( bot.message, u'업로드 시작', u'\r\n' )
	elif szType == 'uploaded':
		bot.message = bot.AppendMessage( bot.message, u'%s' % ( os.path.basename( hData[1] ).replace( '.mp3', '' ) ), u'\r\n' )
	elif szType == 'finish':
		bot.message = bot.AppendMessage( bot.message, u'작업 완료', u'\r\n' )
	else:
		raise Exception( 'invalid type' )
	return

class TbCommandMusicExplorer( CommandBase ):
	def __init__( self, dictSpec ):
		CommandBase.__init__( self, dictSpec )
		self.nPageSize = 5
		return

	def Run( self, message, basetext ):
		if self.RunDefault( message ):
			return

		if self.argc not in [1, 2]:
			self.SendHelp( message )
			return

		msg = u''
		menu = None

		arg = self.argv[0]
		if arg.startswith( '-' ):

			# 버튼 콜백 - 취소하기
			if arg.lower() == '-exit':
				msg += u'취소되었습니다.'

			# 버튼 콜백 - 엘범 반영하기
			if arg.lower() == '-commit':
				mxp = self.bot.user.GetSession( message.chat.id, 'mxp' )

				msg += u'%s\r\n' % ( mxp.artist['name'] )
				msg += u'%s (%s)\r\n' % ( mxp.album['album_name'], mxp.album['date'] )
				message = self.bot.EditMessage( message, msg, menu )

				self.bot.message = message
				mxp.SetOnCommit( OnCommit, self.bot )
				mxp.Commit()
				return

			# 버튼 콜백 - 마지막 아티스트 검색 결과 보기
			elif arg.lower() == '-al':
				mxp = self.bot.user.GetSession( message.chat.id, 'mxp' )

				# 검색 결과로 버튼 레이아웃 만들기
				listArtistButton = []
				if len( mxp.artists ):
					for artist in mxp.artists:
						listArtistButton.append( [ self.InlineButton(
							u'%s' % artist['artist_name'],
							u'/mxp_-sa_%d' % artist['artist_index']
						) ] )
					msg += u'검색된 아티스트 %d건' % len( mxp.artists )
				else:
					msg += u'검색결과가 없습니다.'

				listArtistButton.append( self.GetControlMenu() )
				menu = self.InlineLayout( listArtistButton )

			# 버튼 콜백 - 선택된 아티스트의 엘범 목록 보기
			elif arg.lower() == '-sa':
				mxp = self.bot.user.GetSession( message.chat.id, 'mxp' )

				artist_index = int( self.argv[1] )

				artist_cur = None
				for artist in mxp.artists:
					if artist['artist_index'] == artist_index:
						artist_cur = artist

				if artist_cur is None:
					msg += u'선택퇸 아티스트 : %d\r\n' % artist_index
					msg += u'아티스트 정보를 찾을 수 없습니다.(Invalid Index)'
				else:
					mxp.SelectArtist( artist_index )
					albums = mxp.GetAlbumList()
					album_count = len( albums )

					# 검색 결과로 버튼 레이아웃 만들기
					listAlbumButton = []
					bPaging = ( 10 < album_count )

					for i in range( album_count ):
						if bPaging and i == 5:
							break
						album = albums[i]
						#print( '%2d: %s - %s' % ( album['album_index'], album['album_date'], album['album_name'] ) )
						listAlbumButton.append( [ self.InlineButton(
							u'(%s)%s' % ( album['album_date'], album['album_name'] ),
							u'/mxp_-sal_%d' % album['album_index']
						) ] )

					if 10 < album_count:
						listAlbumButton.append( self.GetAlbumControlMenu() )
					else:
						listAlbumButton.append( self.GetControlMenu() )
					menu = self.InlineLayout( listAlbumButton )

					msg += u'선택퇸 아티스트 : %s\r\n' % artist_cur['artist_name']
					msg += u'%d개의 앨범\r\n' % album_count
					if bPaging:
						msg += u'페이지 : %d / %d' % ( 1, ( album_count + self.nPageSize - 1 ) // self.nPageSize )
						mxp.SetCurrentAlbumPage( 0 )

			# 버튼 콜백 - 선택된 아티스트의 엘범 목록 보기
			elif arg.lower() == '-all':
				mxp = self.bot.user.GetSession( message.chat.id, 'mxp' )
				albums = mxp.GetAlbumList()
				album_count = len( albums )
				nPage = mxp.GetCurrentAlbumPage()
				nMaxPage = ( album_count + self.nPageSize - 1 ) // self.nPageSize

				try:
					move = self.argv[1]
				except:
					move = None

				listAlbumButton = []

				if move == '-init':
					nPage = 0
				elif move == '-fini':
					nPage = nMaxPage - 1
				elif move == '-next':
					nPage += 1
					if nMaxPage <= nPage:
						nPage = nMaxPage - 1
				elif move == '-prev':
					nPage -= 1
					if nPage < 0:
						nPage = 0

				nBase = nPage * self.nPageSize

				for i in range( self.nPageSize ):
					try:
						album = albums[i+nBase]
					except IndexError:
						break
					#print( '%2d: %s - %s' % ( album['album_index'], album['album_date'], album['album_name'] ) )
					listAlbumButton.append( [ self.InlineButton(
						u'(%s)%s' % ( album['album_date'], album['album_name'] ),
						u'/mxp_-sal_%d' % album['album_index']
					) ] )

				listAlbumButton.append( self.GetAlbumControlMenu() )
				menu = self.InlineLayout( listAlbumButton )

				msg += u'%d개의 앨범\r\n' % album_count
				msg += u'페이지 : %d / %d' % ( nPage + 1, ( album_count + self.nPageSize - 1 ) // self.nPageSize )
				mxp.SetCurrentAlbumPage( nPage )


			# 버튼 콜백 - 선택된 엘범의 트렉 목록 보기
			elif arg.lower() == '-sal':
				mxp = self.bot.user.GetSession( message.chat.id, 'mxp' )

				album_index = int( self.argv[1] )
				mxp.SelectAlbum( album_index )
				mxp.QuickMatchAlbum()
				mxp.ShowCurrentTask()
				nDiscCount = mxp.listTrack[0]['disc_total']
				nNotTaggedDirLen = len( mxp.szNotTagged )

				msg += u'%s\r\n' % ( mxp.artist['name'] )
				msg += u'%s (%s)\r\n' % ( mxp.album['album_name'], mxp.album['date'] )
				if 1 < nDiscCount:
					msg += u'%d Tracks (%d Discs)\r\n' % ( len( mxp.listTrack ), nDiscCount )
				else:
					msg += u'%d Tracks\r\n' % ( len( mxp.listTrack ) )

				for i in range( len( mxp.listTrack ) ):
					track = mxp.listTrack[i]

					# 파일 매칭여부
					szMatchedFile = mxp.GetMatched( i )
					if szMatchedFile:
						szMatchedSymbol = u'O'
					else:
						szMatchedSymbol = u'X'

					if 1 < nDiscCount:
						msg += u'%s|%d-%02d|%s\r\n' % ( szMatchedSymbol, track['disc'], track['no'], track['name'] )
					else:
						msg += u'%s|%02d|%s\r\n' % ( szMatchedSymbol, track['no'], track['name'] )
					if szMatchedFile:
						msg += u'\t%s\r\n' % ( szMatchedFile[nNotTaggedDirLen+1:].replace( '.mp3', '' ) )

				listTrackButton = []
				listTrackButton.append( self.GetTrackControlMenu() )
				listTrackButton.append( self.GetControlMenu() )
				menu = self.InlineLayout( listTrackButton )

		# 일반 입력 - 검색하고 가수 목록 보여주기
		else:
			mxp = self.bot.user.GetSession( message.chat.id, 'mxp' )
			if mxp is None:
				mxp = Mxplorer()
				mxp.SetNotTaggedDirectory( os.path.join( 'D:\\', 'Storage', 'STREAM', '_NotTagged' ) )
				mxp.SetServiceDirectory( os.path.join( 'D:\\', 'Storage', 'STREAM_SERVICE' ) )
				self.bot.user.SetSession( message.chat.id, 'mxp', mxp, 60*10 )

			# 검색어 보내기
			keyword = arg

			# 검색하고 결과 저장하기
			mxp.SetFoundArtistList( keyword )

			# 검색 결과로 버튼 레이아웃 만들기
			listArtistButton = []
			if len( mxp.artists ):
				for artist in mxp.artists:
					listArtistButton.append( [ self.InlineButton(
						u'%s' % artist['artist_name'],
						u'/mxp_-sa_%d' % artist['artist_index']
					) ] )
				msg += u'검색어: %s\r\n%d개의 검색결과' % ( keyword, len( mxp.artists ) )
			else:
				msg += u'검색어: %s\r\n검색결과가 없습니다.' % keyword

			listArtistButton.append( self.GetControlMenu() )
			menu = self.InlineLayout( listArtistButton )

		# 메시지 보내기
		if not basetext:
			self.bot.SendMessage( message.chat.id, msg, menu )
		else:
			self.bot.EditMessage( message, msg, menu )

		return

	def GetControlMenu( self ):
		listButton = []
		listButton.append( self.InlineButton( u'\U0000274C', u'/mxp_-exit' ) )
		return listButton

	def GetAlbumControlMenu( self ):
		listButton = []
		listButton.append( self.InlineButton( u'\U000023EA', u'/mxp_-all_-init' ) )
		listButton.append( self.InlineButton( u'\U000025C0', u'/mxp_-all_-prev' ) )
		listButton.append( self.InlineButton( u'\U000025B6', u'/mxp_-all_-next' ) )
		listButton.append( self.InlineButton( u'\U000023E9', u'/mxp_-all_-fini' ) )
		listButton.append( self.InlineButton( u'\U0001F465', u'/mxp_-al' ) )
		listButton.append( self.InlineButton( u'\U0000274C', u'/mxp_-exit' ) )
		return listButton

	def GetTrackControlMenu( self ):
		listButton = []
		listButton.append( self.InlineButton( u'\U0001F465', u'/mxp_-al' ) )
		listButton.append( self.InlineButton( u'\U0001F4BF', u'/mxp_-all' ) )
		listButton.append( self.InlineButton( u'\U0001F197', u'/mxp_-commit' ) )
		return listButton


	def OnHelp( self ):
		msg = self.GetDefaultHelp() + u'\r\n'
		msg += self.MakeSubHelp( u'/mxp (검색어)'	, u'엘범이나 아티스트를 검색합니다.'	)
		return msg


if __name__ == '__main__':
	print( 'test' )

