#-*- coding: utf-8 -*-

import sys
import os
import logging
import logging.handlers
import threading
import traceback
from time import sleep

sys.path.insert( 0, os.path.join( os.path.dirname( os.path.realpath(__file__) ), '..', 'pyutil' ) )
from PuInstImport import InstImport

telegram = InstImport( 'python-telegram-bot', 'telegram' )
from telegram.error import NetworkError, Unauthorized


class TelegramBot:
	def __init__( self, token, admin, name, nolog = False ):
		self.hCore = telegram.Bot( token )
		self.nAdmin = admin
		self.nUpdateId = 0
		self.szBotName = name
		self.bTerminate = False

		self.LogInit( nolog )

		return

	def LogInit( self, nolog ):
		if nolog:
			self.log = None
			return
			
		#logging.basicConfig( filename=self.szBotName + u'.log', format = u'%(asctime)s - %(name)s - %(levelname)s - %(message)s' )

		self.log = logging.getLogger( self.szBotName )
		self.log.setLevel( logging.DEBUG )

		formatter = logging.Formatter( u'%(asctime)s - %(name)s - %(levelname)s - %(message)s' )
		
		fh = logging.handlers.RotatingFileHandler( filename=self.szBotName + u'.log', maxBytes=5*1024*1024, backupCount=100 )
		sh = logging.StreamHandler()

		fh.setFormatter( formatter )
		sh.setFormatter( formatter )

		self.log.addHandler( fh )
		self.log.addHandler( sh )

		return

	def Log( self, text ):
		print( text )
		if self.log is not None:
			self.log.info( str(text) )
			return

	def Start( self ):
		self.hEventOnStart = threading.Event()
		self.hEventOnFinish = threading.Event()

		self.bMainThreadAlive = True
		self.MainThread = threading.Thread( target = self.MainLoop )
		self.MainThread.deamon = True
		self.MainThread.start()

		self.hEventOnStart.wait()
		self.OnStart()
		self.hEventOnFinish.wait()
		self.OnFinish()
		return

	def OnStart( self ):
		pass

	def OnFinish( self ):
		pass

	def SendNotify( self, text ):
		self.SendAdmin( u'[NOTI] ' + text )
		return

	def SendError( self, text ):
		self.SendAdmin( u'[ERRR] ' + text )
		return

	def SendAdmin( self, text ):
		self.SendMessage( self.nAdmin, text )
		return

	def SendDual( self, text ):
		if self.message.chat.id != self.nAdmin:
			self.SendMessage( self.message.chat.id, text )
		self.SendAdmin( text )
		return

	def SendMessage( self, nChatId, text, keyboard = None ):
		self.Log( text )
		retry = 5
		result = None
		while 0 < retry:
			try:
				result = self.hCore.sendMessage( chat_id = nChatId, text = text, parse_mode = telegram.ParseMode.HTML, reply_markup = keyboard )
				break
			except:
				retry -= 1
				continue
		return result

	def EditMessage( self, message, text, keyboard = None ):
		return self.hCore.editMessageText( chat_id=message.chat.id, message_id=message.message_id, text=text, parse_mode = telegram.ParseMode.HTML, reply_markup = keyboard )

	def AppendMessage( self, message, text, connector=u'', keyboard = None ):
		new_text = message.text + connector + text
		return self.EditMessage( message, new_text, keyboard )

	def MessageHandler( self, message, basetext ):
		pass

	def CallbackHandler( self, callback ):
		pass

	def MainLoop( self ):
		try:
			self.nUpdateId = self.hCore.get_updates()[-1].update_id + 1
		except IndexError:
			self.nUpdateId = None

		self.SendNotify( self.szBotName + u' 서비스를 시작합니다.' )

		# OnStart 를 호출하도록 Signal
		self.hEventOnStart.set()

		while self.bMainThreadAlive == True:
			try:
				for update in self.hCore.get_updates( offset = self.nUpdateId, timeout = 10 ):
					self.nUpdateId = update.update_id + 1
					if update.message:
						if not self.MessageHandler( update.message, None ):
							self.bMainThreadAlive = False
					elif update.callback_query:
						if not self.CallbackHandler( update.callback_query ):
							self.bMainThreadAlive = False
					else:
						self.SendError( u'Invalid Message' )
						self.Log( update )
						self.Log( '-------------------' )

			except NetworkError:
				sleep(1)

			except Unauthorized:
				self.nUpdateId += 1
			except:
				self.bMainThreadAlive = False
				self.bTerminate = True
				traceback.print_exc()
				self.SendNotify( u'오류발생.' )

		self.SendNotify( self.szBotName + u' 서비스를 종료합니다.' )
		self.hEventOnFinish.set()
		return


if __name__ == '__main__':
	print( '---------------------------------' )
	bot = TelegramBot( u'473687016:AAE0yN2SW_GYURI8XouODxftiYpSYk3pz9w', 52506654, u'Eightanium-Text' )
	print( '---------------------------------' )
	bot.Start()
	print( '---------------------------------' )


