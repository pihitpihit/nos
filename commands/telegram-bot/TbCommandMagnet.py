#-*- coding: utf-8 -*-

import sys, os
from TbCommandBase import CommandBase

sys.path.insert( 0, os.path.join( "D:\\", "Develop", "commands", "uTorrent-Remover-master" ) )
import Register



class TbCommandMagnet(CommandBase):
	def __init__( self, dictSpec ):
		CommandBase.__init__( self, dictSpec )
		return

	def Run( self, message, basetext ):
		if self.RunDefault( message ):
			return
		
		# ref. magnet:?xt=urn:btih:C7142D432883D9B46FF7DFDD2A9D9EC73CB9CD88

		# prefix 확인
		if not message.text.startswith( u'magnet:?xt=urn:btih:' ):
			self.SendFormatError( message.chat.id )
			return

		# 길이 확인
		if len( message.text ) < 60:
			self.SendFormatError( message.chat.id )
			return

		# 저장소 확인
		szStorage = self.bot.user.GetStorage( message.chat.id )
		if not szStorage:
			self.SendStorageError( message.chat.id )
			return

		# 등록 시도
		szPath = Register.Register( message.text, szStorage )
		if szPath == None:
			self.SendRegistrationError( message.chat.id )
			return

		# 상위 경로 숨김
		szPath = szPath[ len( u'D:\\Storage\\' ) : ]

		# 등록 완료
		self.bot.SendMessage( message.chat.id, u'<b>[마그넷 등록 완료]</b>\r\n경로: %s\r\n내 대기열 확인하기: /torr_list\r\n전체 대기열 확인하기: /torr_listall' % ( szPath ) )
		return

	def SendRegistrationError( self, chatid ):
		self.bot.SendMessage( chatid, u'[오류] 등록에 실패했습니다.\r\n문제가 계속 발생할 시 관리자에게 문의해주세요.' )
		return

	def SendFormatError( self, chatid ):
		self.bot.SendMessage( chatid, u'[오류] 잘못된 마그넷 주소입니다.\r\n문제가 계속 발생할 시 관리자에게 문의해주세요.' )
		return

	def SendStorageError( self, chatid ):
		self.bot.SendMessage( chatid, u'[오류] 저장소가 할당되지 않아 마그넷을 등록할 수 없습니다.\r\n관리자에게 문의해주세요.' )
		return



if __name__ == '__main__':
	print( 'test' )

