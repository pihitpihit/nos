#-*- coding:utf-8 -*-

#
#	Music Utils
#

import json
import pickle
import requests
import traceback
from bs4 import BeautifulSoup

import urllib
from urllib.parse import urlencode

headers = { 'User-Agent': 'Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko' }
melon_base_url = 'https://www.melon.com'
melon_artist_url_format = melon_base_url + '/artist/detail.htm?artistId=%s'
melon_song_url_format = melon_base_url + '/song/detail.htm?songId=%s'
melon_album_url_format = melon_base_url + '/album/detail.htm?albumId=%s'
melon_album_list_url_format = melon_base_url + '/artist/album.htm?artistId=%s'
melon_album_list_url_format_total = melon_base_url + '/artist/albumPaging.htm?startIndex=1&pageSize=%s&listType=0&orderBy=ISSUE_DATE&artistId=%s'

naver_base_url = 'https://music.naver.com'
naver_artist_search_url_format = naver_base_url + '/search/search.nhn'
naver_artist_url_format = naver_base_url + '/artist/intro.nhn'
naver_album_list_url_format = naver_base_url + '/artist/album.nhn'
naver_album_url_format = naver_base_url + '/album/index.nhn'

#
# Beatutiful Music
#
class BeautifulMusic:
	def __init__( self, szContents ):
		self.bs = BeautifulSoup( szContents, 'html.parser' )
		self.ClearLast()
		self.last_stack = []
		return

	def ClearLast( self ):
		self.last = self.bs
		return self

	def PushLast( self ):
		self.last_stack.append( self.last )
		return

	def SetLast( self, last ):
		self.last = last
		return self

	def PopLast( self, bRemove = False ):
		if len( self.last_stack ):
			self.last = self.last_stack[-1]
			if bRemove:
				del( self.last_stack[-1] )
		else:
			self.ClearLast()
		return self

	def GetClass( self, szClassName ):
		self.last = self.last.find( class_ = szClassName )
		return self

	def GetTag( self, szTag ):
		self.last = self.last.find( szTag )
		return self

	def GetTags( self, szTag ):
		self.last = self.last.findAll( szTag )
		return self

	def GetClassList( self, szClassName ):
		self.last = self.last.findAll( class_ = szClassName )
		return self

	def ForEachLast( self, callback, userdata, count = 0 ):
		if isinstance( self.last, list ):
			index = 0
			for last in self.last:
				callback( index, last, userdata )
				index += 1

				#개수 제한
				if 0 < count and count <= index:
					break
		else:
			callback( 0, self.last, userdata )

		return self

	def GetDtDdList( self ):
		list_dtdd = []
		index = 0

		while True:
			index += 1
			dt = self.last.select_one( 'dt:nth-child(%d)' % index )
			if dt is None: break

			index += 1
			dd = self.last.select_one( 'dd:nth-child(%d)' % index )
			if dd is None: break

			list_dtdd.append( [ dt, dd ] )

		return list_dtdd

	def Select( self, szSelector ):
		self.last = self.last.select_one( szSelector )
		return self

	def BrToNewline( self ):
		for br in self.last.find_all( 'br' ):
			br.replace_with( '\n' )
		return self

	def Text( self, bBrStrip = True ):
		if bBrStrip:
			self.BrToNewline()
		return self.last.text.strip()

	def Print( self ):
		print( self.Text() )
		return
#
# Utils
#
def UtilRemoveTrailer( szInString, szPivot ):
	return szInString.split( szPivot )[0] + szPivot
def UtilGetTrailer( szInString, szPivot ):
	return szInString.split( szPivot )[1].strip()
def UtilGetInnerString( szInString, szMarkerLeft, szMarkerRight ):
	return szInString.split( szMarkerLeft )[1].split( szMarkerRight )[0].strip()

#
# 노래 검색 기능 예제 (멜론)
#
class MusicUtil:
	def __init__( self, hExternalNaver = None ):
		self.naver = hExternalNaver
		return

	def MuFind( self, szKeyword ):
		url = melon_base_url + '/search/keyword/index.json'
		params = {
			'jscallback' 	: 'jQuery19107160083217126991_1569072907064',
			'query'			: szKeyword,
		}

		response = requests.get( url, headers = headers, params = params ).text
		json_string = response.replace( params['jscallback'] +  '(', '' ).replace( ');', '' )
		result_dict = json.loads( json_string )


		for song in result_dict['SONGCONTENTS']:
			print( '{ARTISTNAME} - {SONGNAME} - {ALBUMNAME}'.format( **song ) )
			song_url = melon_artist_url_format%song['SONGID']
			print( '\t - ' + song_url.format( **song ) )
		return

	def MuCbGetNaverAtristSimple( self, index, bs, data ):
		info = {}
		info['artist_index'] = index
		try:
			info['artist_id'] = UtilGetTrailer( bs.a.get( 'href' ), 'artistId=' )
		except:
			if 'musicianId=' in bs.a.get( 'href' ):
				return
			else:
				raise
		info['artist_img'] = UtilRemoveTrailer( bs.a.img.get( 'src' ), '.jpg' )
		info['artist_name'] = bs.parent.dl.dt.text.strip()
		data.append( info )
		return

	def MuCbGetNaverArtistMembers( self, index, bs, data ):
		data.append( bs.text.strip() )
		return

	def MuCbGetAlbumSimple( self, index, bs, data ):
		album_info = {}

		album_info['album_index'] = len( data )
		album_info['album_id'] = UtilGetTrailer( bs.a.get( 'href' ), 'albumId=' )
		album_info['album_img'] = UtilRemoveTrailer( bs.a.span.img.get( 'src' ), '.jpg' )
		album_info['album_name'] = bs.a.p.strong.text.strip()
		album_info['album_artist'] = bs.find( class_ = 'name' ).text.strip()
		album_info['album_date'] = bs.find( class_ = 'date' ).text.strip()

		data.append( album_info )
		return

	#
	# 가수 검색 ( 상위 10건만 노출 )
	#
	def MuFindArtist( self, szKeyword, nCount = 10 ):
		url = naver_artist_search_url_format
		params = {
			'target': 'artist',
			'query' : szKeyword,
		}

		if self.naver:
			self.naver.Goto( url + '?' + urlencode( params ) )
			html = self.naver.GetPage()
		else:
			html = requests.get( url, headers = headers, params = params ).text
		bm = BeautifulMusic( html )

		atrist_list = []
		try:
			bm.GetClass( 'lst_detail6' ).GetClassList( 'thumb pht103' ).ForEachLast( self.MuCbGetNaverAtristSimple, atrist_list, nCount )
		except:
			traceback.print_exc()
			atrist_list = []
			

		return atrist_list

	#
	# 가수 정보 획득
	#
	def MuGetArtistInfo( self, szArtistId ):
		url = naver_artist_url_format
		params = { 'artistId': szArtistId }

		if self.naver:
			self.naver.Goto( url + '?' + urlencode( params ) )
			html = self.naver.GetPage()
		else:
			html = requests.get( url, headers = headers, params = params ).text
		bm = BeautifulMusic( html )

		artist_info = {}
		artist_info['id'] = szArtistId
		artist_info['name'] = bm.ClearLast().GetClass( 'prof_area' ).last.div.h2.text.strip()
		artist_info['img'] = UtilRemoveTrailer( bm.last.span.span.img.get( 'src' ), '.jpg' )

		bm.ClearLast().GetClass( 'mscn_info_lst' ).PushLast()
		#print( bm.last.div.div.dl )

		# 기본정보
		keys = {
			'출생':'birth',
			'데뷔':'debut',
			'유형':'type',
			'연대':'period',
			'장르':'genre',
			'스타일':'style',
			'멤버':'member',
			'소속그룹':'group',
			'사이트':'site',
		}

		dtdd = bm.SetLast( bm.last.div.div.dl ).GetDtDdList()
		for key, value in dtdd:
			key = key.text.strip()
			if key in keys:
				if key in [ '멤버', '소속그룹' ]:
					members = []
					bm.SetLast( value ).GetTags( 'a' ).ForEachLast( self.MuCbGetNaverArtistMembers, members ).PopLast()
					value = ', '.join( members )
					#value = value.text
				else:
					value = value.text.strip()
				artist_info[keys[key]] = value
			else:
				raise 'key(%s) not found' % key

		# 바이오그래피
		try:
			artist_info['introduction'] = bm.PopLast().GetClass( 'biography' ).GetClass( 'dsc full' ).Text()
		except:
			artist_info['introduction'] = ''

		# 수상 내역
		try:
			artist_info['award_history'] = ''
			dtdd = bm.PopLast().GetClass( 'award' ).GetClass( 'prize' ).GetClass( 'dl_lst' ).GetDtDdList()
			for year, desc in dtdd:
				year = year.text.strip()
				desc = desc.text.strip()
				if artist_info['award_history'] == '':
					artist_info['award_history'] += year + ': ' + desc
				else:
					artist_info['award_history'] += '\n' + year + ' | ' + desc
		except:
			artist_info['award_history'] = ''
			

		# 앨범 목록
		listAlbum = []
		nAlbumPerPage = 20
		nAlbumCount = self.MuGetAlbumList( szArtistId, 1, listAlbum )
		nPageCount = int( ( nAlbumCount + nAlbumPerPage -1 ) / nAlbumPerPage )

		for nPage in range( 2, nPageCount + 1 ):
			self.MuGetAlbumList( szArtistId, nPage, listAlbum )

		artist_info['album_count'] = nAlbumCount
		artist_info['album_list'] = listAlbum

		return artist_info

	#
	# 앨범 목록 획득
	#
	def MuGetAlbumList( self, szArtistId, nPage, listAlbum ):
		url = naver_album_list_url_format
		params = { 'artistId': szArtistId, 'filteringOptions': 'ALL', 'sorting': 'newRelease', 'page': nPage }

		if self.naver:
			self.naver.Goto( url + '?' + urlencode( params ) )
			html = self.naver.GetPage()
		else:
			html = requests.get( url, headers = headers, params = params ).text
		bm = BeautifulMusic( html )

		# album count
		nAlbumCount = None
		if nPage == 1:
			nAlbumCount = int( bm.GetClass( 'sort_group' ).last.div.ul.li.a.text.split( ' ' )[1].strip() )

		# album info
		bm.ClearLast().GetClass( 'album_lst' ).GetClassList( 'thmb_cover' ).ForEachLast( self.MuCbGetAlbumSimple, listAlbum )
		
		return nAlbumCount

	#
	# 앨범 정보 획득
	#
	def MuGetAlbumInfo( self, szAlbumId ):
		url = naver_album_url_format
		params = { 'albumId': szAlbumId }

		if self.naver:
			self.naver.Goto( url + '?' + urlencode( params ) )
			html = self.naver.GetPage()
		else:
			html = requests.get( url, headers = headers, params = params ).text
		bm = BeautifulMusic( html )

		album_info = {}

		album_info['album_id'] = szAlbumId
		album_info['album_img'] = UtilRemoveTrailer( bm.GetClass( 'thumb pht217' ).last.a.img.get( 'src' ), '.jpg' )
		album_info['album_name'] = bm.ClearLast().GetClass( 'info_txt' ).Select( 'h2' ).Text()

		dtdd = bm.ClearLast().GetClass( 'desc' ).GetDtDdList()
		
		keys = {
			'아티스트':'artist',
			'장르':'genre',
			'발매':'date',
			'배급':'release',
		}
		for key, value in dtdd:
			album_info[ keys[ key.text.strip() ] ] = value.text.strip()

		album_info['track_count'] = int( UtilGetInnerString( bm.ClearLast().GetClass( 'section_tit' ).GetClass( 'desc type_num' ).Text(), '(', ')' ) )
		album_info['track_list'] = []
		album_info['disc_count'] = 0
		album_info['track_num_adj'] = 0

		table = bm.ClearLast().GetClass( '_tracklist_mytrack tracklist_table' ).last.table.tbody
		bm.SetLast( table ).GetTags( 'tr' ).ForEachLast( self.MuCbGetSongSimple, album_info )

		# if single disc album
		if album_info['disc_count'] == 0:
			album_info['disc_count'] = 1

		# update total disc field
		for track in album_info['track_list']:
			track['disc_total'] = album_info['disc_count']

		return album_info

	def MuGetSongDetail( self, szTrackId ):
		url = 'https://music.naver.com/lyric/index.nhn'
		params = { 'trackId': szTrackId }

		if self.naver:
			self.naver.Goto( url + '?' + urlencode( params ) )
			html = self.naver.GetPage()
		else:
			html = requests.get( url, headers = headers, params = params ).text
		bm = BeautifulMusic( html )
		
		song_info = {}
		try:
			song_info['lyric'] = bm.ClearLast().GetClass( 'show_lyrics' ).Text()
		except:
			song_info['lyric'] = ''

		song_info['artist'] = None
		if song_info['artist'] is None:
			try:
				song_info['artist'] = bm.ClearLast().GetClass( 'artist' ).last.a.get( 'title' ).strip()
			except:
				pass
		if song_info['artist'] is None:
			try:
				song_info['artist'] = bm.ClearLast().GetClass( 'artist' ).Text()
			except:
				raise
		
		#song_info['etc'] = bm.ClearLast().GetClass( 'song_info' ).Text()
		return song_info

	def MuCbGetSongSimple( self, index, bs, album_info ):
		if index == 0:
			return

		data = album_info['track_list']

		song_info = {}

		# if there is no name tag, check DiscNumMarker
		tag_name = bs.find( class_ = 'name' )
		if tag_name is None:
			cd_num = bs.find( class_ = 'cd_divide' ).text
			if cd_num.startswith( 'CD ' ):
				album_info['disc_count'] += 1
				album_info['track_num_adj'] = index
				return
			else:
				raise 'not expected CD divide format'

		# track attributes
		bIsTitle = bs.find( class_ = '_ico_title ico_title' ) is not None
		bIs19 = bs.find( class_ = '_ico_19 ico19' ) is not None

		if bIs19:
			tag = bs.find( class_ = 'name' ).select_one( 'strong:nth-child(5)' )
		else:
			tag = bs.find( class_ = 'name' ).select_one( 'strong:nth-child(4)' )
		# if title
		if tag is not None:
			tag = tag.a.text.strip()
		else:
			# if not title
			if bIs19:
				tag = bs.find( class_ = 'name' ).select_one( 'a:nth-child(4)' )
			else:
				tag = bs.find( class_ = 'name' ).select_one( 'a:nth-child(3)' )
			if tag is not None:
				tag = tag.text.strip()
			else:
				# if CD only
				tag = bs.find( class_ = 'name' ).select_one( 'span:nth-child(3)' )
				if tag is not None:
					tag = tag.text.strip()
				else:
					raise 'not expected name format'

		song_info['no'] = index - album_info['track_num_adj']
		song_info['name'] = tag
		if album_info['disc_count'] == 0:
			song_info['disc'] = 1
		else:
			song_info['disc'] = album_info['disc_count']
		song_info['trackid'] =bs.get( 'trackdata' ).split( '|' )[0]
		song_info.update( self.MuGetSongDetail( song_info['trackid'] ) )

		data.append( song_info )
		return

	def MuGetArtistTotal( self, szArtistId ):
		artist_info = self.MuGetArtistInfo( szArtistId )

		nIndex = 1
		album_info = {}

		for album in artist_info['album_list']:
			print( nIndex, album )
			info = {}
			id = album['album_id']
			info['album_num'] = nIndex
			info['album_info'] = self.MuGetAlbumInfo( id )
			album_info[id] = info
			nIndex += 1
			#if nIndex == 3: break

		artist_info['album_info'] = album_info

		return artist_info


#import eyed3


if __name__ == '__main__':
	#info = MuGetArtistInfo( 170118 )
	#info = MuGetArtistInfo( 35551 )
	#info = MuGetAlbumInfo( 562394 )
	#info = MuGetAlbumInfo( 1962881 )
	
	#f1 = open( '01.SOLO.mp3', 'r' )
	#f2 = open( '01.SOLO.mp3', 'r+b' )
	
	if False:
		info = MuGetArtistTotal( 500571 )
		f = open( 'file.pkl', 'wb' )
		pickle.dump( info, f )
		f.close()
		
		f = open( 'file.pkl', 'rb' )
		info2 = pickle.load( f )
		print( info2 )
		print( info == info2 )
	else:
		info = MuGetAlbumInfo( 2623361 )
		with open( 'lyrics.txt', 'w' ) as f:
			f.write( info['track_list'][0]['lyric'] )

		print( info['track_list'][0]['lyric'] )

		# f = open( 'lyrics.txt', 'r' )
		# ReadLyirsFile = f.read()
		# #LyirsFile = unicode( ReadLyirsFile, 'utf-8' )
		# audio = eyed3.load( '01.SOLO.mp3' )
		# audio.tag.lyrics.set( ReadLyirsFile )
		# audio.tag.artist = u"Integrity"
		# audio.tag.save()


