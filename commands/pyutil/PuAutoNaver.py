#-*- coding: utf-8 -*-

import sys, os
import traceback
sys.path.insert( 0, os.path.join( '..', 'pyutil' ) )
from PuInstImport import InstImport
from PuWebBrowser import WebBrowser
requests = InstImport( 'requests' )

URL = 'https://nid.naver.com/nidlogin.login?mode=form&url=https%3A%2F%2Fwww.naver.com'

class AutoNaver:
	def __init__( self, szNameAlias = 'Test' ):
		self.bro = WebBrowser( 'EightAutoNaver-' + szNameAlias )
		self.bro.Start()
		return

	def __fini__( self ):
		self.bro.End()
		return

	def LogIn( self, szId, szPw ):
		self.Goto( URL )

		if not self.bro.LoadCookie():
			script = "(function execute(){\
document.querySelector( '#id' ).value = '" + szId + "';\
document.querySelector( '#pw' ).value = '" + szPw + "';\
})();"

			self.bro.ExecuteScript( script )
			element = self.bro.WaitForElement( 10, 'input.btn_global' )
			element.click()
			element = self.bro.WaitForElement( 10, 'span.btn_upload' ) # btn_cancel
			element.click()
			self.bro.SaveCookie()
		return

	def Goto( self, szUrl ):
		self.bro.Goto( szUrl )
		return

	def GetPage( self ):
		return self.bro.GetPage()

if __name__ == '__main__':
	an = AutoNaver()
	an.LogIn( 'fertlizer', '00dkskANA!@' )
	print( 'AutoNaver' )
