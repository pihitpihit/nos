#-*- coding:utf-8 -*-

#
#	Music Explorer
#

# pip install python-magic-bin==0.4.14
# pip install pillow
# pip install eyed3
# pip install beautifulsoup4

import os
import sys
import unicodedata
import eyed3
import logging
import shutil
from PIL import Image
from urllib.request import urlretrieve
from MusicUtils import *
from difflib import SequenceMatcher
from PuAutoNaver import *

class Mxplorer:
	def __init__( self ):
		self.artists = None
		self.artist = None
		self.albums = None
		self.album = None

		self.szNotTagged = None
		self.szServiceDir = None

		self.listNotTagged = None
		self.listTrack = None
		self.nCurrentTrack = None
		self.listSkipped = []

		self.fnOnCommit = None
		self.hOnCommit = None

		self.naver = AutoNaver( 'Mxplorer' )
		self.naver.LogIn( 'fertlizer', '00dkskANA!@' )
		self.mu = MusicUtil( self.naver )
		return

	def __fini__( self ):
		self.naver.End()

	def SetServiceDirectory( self, szServiceDir ):
		self.szServiceDir = szServiceDir
		return

	def SetNotTaggedDirectory( self, szNotTagged ):
		self.szNotTagged = szNotTagged
		self.GetNotTaggedList()
		return

	def GetNotTaggedListByPath( self, szPath ):
		listNotTagged = []
		for entry in os.listdir( szPath ):
			entry_path = os.path.join( szPath, entry )
			if os.path.isdir( entry_path ):
				listNotTagged.extend( self.GetNotTaggedListByPath( entry_path ) )
			else:
				listNotTagged.append( entry_path )

		return listNotTagged

	def GetNotTaggedList( self ):
		self.listNotTagged = self.GetNotTaggedListByPath( self.szNotTagged )
		return self.listNotTagged

	def ShowNotTaggedList( self ):
		self.GetNotTaggedList()
		for i in range( len( self.listNotTagged ) ):
			print( '%2d: %s' % ( i, self.listNotTagged[i] ) )
		return

	def SetFoundArtistList( self, keyword ):
		self.artists = self.mu.MuFindArtist( keyword )
		return

	def SelectArtist( self, index ):
		self.artist = self.mu.MuGetArtistInfo( self.artists[index]['artist_id'] )
		self.albums = self.artist['album_list']
		return

	def ShowArtistInfo( self ):
		listKey = [ 'id', 'name', 'img', 'debut', 'type', 'period', 'genre', 'style', 'member', 'album_count' ]
		szFormat = '%%-%ds: %%s' % len( max( listKey, key=len ) )
		for key in listKey:
			print( szFormat % ( key, self.artist[key] ) )
		return

	def GetAlbumList( self ):
		return self.albums

	def ShowAlbumList( self ):
		for i in range( len( self.albums ) ):
			album = self.albums[i]
			print( '%2d: %s - %s' % ( album['album_index'], album['album_date'], album['album_name'] ) )
		return

	def SelectAlbum( self, index ):
		self.album = self.mu.MuGetAlbumInfo( self.albums[index]['album_id'] )
		self.listTrack = self.album['track_list']
		self.listMatched = [ None ] * len( self.listTrack )
		self.nCurrentTrack = None
		self.listSkipped = []
		return

	def GetAlbumDirectory( self ):
		try:
			szPath = os.path.join( self.szServiceDir, self.artist['name'][0], self.artist['name'], '(%s) %s'%( self.album['date'], self.album['album_name'] ) )
			return AdjustPath( szPath )
		except:
			return None

	def ShowTackList( self ):
		print( 'IDX|DISC|TRCK| TITLE' )
		for i in range( len( self.listTrack ) ):
			print( '%2d | %2d | %2d | %s'% ( i, self.listTrack[i]['disc'], self.listTrack[i]['no'], self.listTrack[i]['name'] ) )
		return

	def SelectTrack( self, index ):
		self.nCurrentTrack = index
		return

	def SkipTrack( self, index ):
		self.listSkipped.append( index )
		return

	def AssignNotMatched( self, index ):
		self.listMatched[self.nCurrentTrack] = ( self.listNotTagged[index], 0 )
		return

	def ShowCurrentTask( self ):
		print( 'artist     : %s' % ( self.artist['name'] if self.artist is not None else '(NotSet)' ) )
		print( 'album      : %s' % ( self.album['album_name'] if self.album is not None else '(NotSet)' ) )
		szDirectory = self.GetAlbumDirectory()
		print( 'directory  : %s' % ( szDirectory if szDirectory is not None else '(NotSet)' ) )
		if self.listTrack is None:
			print( 'track list : (NotSet)')
		else:
			nMaxTrackNameLen = len( 'Track Name' )
			for track in self.listTrack:
				nLen = StrWidth( track['name'] )
				if nMaxTrackNameLen < nLen:
					nMaxTrackNameLen = nLen

			print( 'track list :')
			szFormat = '  | IDX|DISC|TRCK| %%-%ds | %%s' % ( nMaxTrackNameLen + len( 'Track Name' ) - StrWidth( 'Track Name' ) )
			print( szFormat % ( 'Track Name', 'Matched' ) )
			for i in range( len( self.listTrack ) ):
				name = self.listTrack[i]['name']
				disc = self.listTrack[i]['disc']
				track = self.listTrack[i]['no']
				szFormat = '  | %%2d | %%2d | %%2d | %%-%ds | %%s' % ( nMaxTrackNameLen + len( name ) - StrWidth( name ) )
				if i in self.listSkipped:
					szFile = '(Skip)'
				elif self.listMatched[i]:
					szFile = '%s(%.1f)' % ( self.listMatched[i][0], self.listMatched[i][1]*100 )
				else:
					szFile = '-'
				print( szFormat % ( i, disc, track, name, szFile ) )
		print( '' )
		return

	def QuickMatchAlbum( self ):
		if self.listTrack is None:
			print( 'track list : (NotSet)')
			return

		listNotTagged = self.GetNotTaggedList()
		listVaccumed = []
		listMatchedFlag = []
		for path in listNotTagged:
			if not path.endswith( '.mp3' ):
				listVaccumed.append( None )
				listMatchedFlag.append( False )
				continue
			path = path[len( self.szNotTagged )+1:]
			# ( szVaccumed, nMatchedTrack, nMatchedScore )
			listVaccumed.append( ( path.replace( ' ', '' ).replace( '\t', '' ), None, 0 ) )
			listMatchedFlag.append( False )
		#print( 'listVaccumed', listVaccumed )

		while True:
			nMatchedCount = 0
			for j in range( len( listVaccumed ) ):
				if listVaccumed[j] is None:
					continue
				if listMatchedFlag[j] is True:
					continue

				nMatched = None
				nMaxScore = 0

				for i in range( len( self.listTrack ) ):
					name = self.listTrack[i]['name']
					name = name.replace( ' ', '' ).replace( '\t', '' )

					nScore = Similar( name, listVaccumed[j][0] )

					if nMaxScore < nScore and ( self.listMatched[i] is None or self.listMatched[i][1] < nScore ):
						nMaxScore = nScore
						nMatched = i
						#self.listMatched[nMatched] = ( listNotTagged[j], nMaxScore )

				if nMatched is not None:
					if self.listMatched[nMatched] is not None:
						nPrevNotTagged = self.listMatched[nMatched][2]
						listMatchedFlag[nPrevNotTagged] = False
					self.listMatched[nMatched] = ( listNotTagged[j], nMaxScore, j )
					listMatchedFlag[j] = True
					nMatchedCount += 1

			break
			if nMatchedCount == 0:
				break
		return

	def SetCurrentAlbumPage( self, nPage ):
		self.nAlbumPage = nPage
		return

	def GetCurrentAlbumPage( self ):
		return self.nAlbumPage

	def GetMatched( self, index ):
		if self.listMatched[index] is not None:
			return self.listMatched[index][0]
		else:
			return None

	def SetOnCommit( self, fnOnCommit, hUserData ):
		self.fnOnCommit = fnOnCommit
		self.hOnCommit = hUserData
		return

	def OnCommit( self, szType, hCommitData ):
		if self.fnOnCommit:
			self.fnOnCommit( szType, hCommitData, self.hOnCommit )
		return

	def Commit( self ):
		# step0. starting
		self.OnCommit( 'start', None )
		szAlbumDir = self.GetAlbumDirectory()
		if not os.path.exists( szAlbumDir ):
			os.makedirs( szAlbumDir )

		# step1. make cover
		self.OnCommit( 'cover', None )
		coverpath = self.CommitCover()
		self.album_cover = GetImageThumbnail( coverpath, 1024 )

		# step2. tagging
		self.OnCommit( 'tag', None )
		for i in range( len( self.listTrack ) ):
			if i in self.listSkipped:
				continue
			self.__Tagging( i )

		# step3. uploading
		self.OnCommit( 'upload', None )
		for i in range( len( self.listTrack ) ):
			if i in self.listSkipped:
				continue
			distpath = os.path.join( szAlbumDir, '%02d-%s.mp3' % ( i + 1, self.listTrack[i]['name'] ) )
			distpath = AdjustPath( distpath )
			print( '[info] move idx-%d, disc-%d, track-%d' % ( i, self.listTrack[i]['disc'], self.listTrack[i]['no'] ) )
			print( '       from : %s' % self.listMatched[i][0] )
			print( '         to : %s' % distpath )
			shutil.move( self.listMatched[i][0], distpath )
			self.OnCommit( 'uploaded', ( i, distpath ) )
		self.listSkipped = []

		# step4. finish
		self.OnCommit( 'finish', None )
		return

	def CommitCover( self ):
		return SaveUrlImage( self.album['album_img'], 2048, self.GetAlbumDirectory() )

	def __Tagging( self, index ):
		szTempFile = os.path.join( os.path.dirname( self.listMatched[index][0] ), 'temp.mp3' )
		shutil.move( self.listMatched[index][0], szTempFile )
		try:
			audio = eyed3.load( szTempFile )

			audio.tag.album = self.album['album_name']
			audio.tag.album_artist = self.album['artist']
			audio.tag.release_date = self.album['date'].replace( '.', '-' )

			audio.tag.artist = self.listTrack[index]['artist']
			audio.tag.title = self.listTrack[index]['name']
			audio.tag.disc_num = ( self.listTrack[index]['disc'], self.listTrack[index]['disc_total'] )
			audio.tag.track_num = self.listTrack[index]['no']
			audio.tag.lyrics.set( self.listTrack[index]['lyric'] )
			audio.tag.images.set( type_=3, img_data=self.album_cover, mime_type='image/jpeg' )
			audio.tag.save()
		except:
			raise
		finally:
			shutil.move( szTempFile, self.listMatched[index][0] )
		return


def Similar( a, b ):
	return SequenceMatcher( None, a, b ).ratio()

def AdjustPath( szPath ):
	if szPath[0].isalpha() and szPath[1] == ':':
		szPath = szPath[0:2] + szPath[2:].replace( ' : ', '：' ).replace( ' :', '：' ).replace( ':', '：' )
	else:
		szPath = szPath.replace( ':', '：' )
	szPath = szPath.replace( ' | ', '｜' ).replace( ' |', '｜' ).replace( '| ', '｜' ).replace( '|', '｜' )
	szPath = szPath.replace( ' ? ', '？' ).replace( ' ?', '？' ).replace( '? ', '？' ).replace( '?', '？' )
	szPath = szPath.replace( '"', '\'' )
	szPath = szPath.replace( ' / ', '／' ).replace( ' /', '／' ).replace( '/ ', '／' ).replace( '/', '／' )
	szPath = szPath.replace( ' * ', '＊' ).replace( ' *', '＊' ).replace( '* ', '＊' ).replace( '*', '＊' )
	szPath = szPath.replace( '[instrumental]', '(Inst.)' )
	szPath = szPath.replace( '[Instrumental]', '(Inst.)' )
	szPath = szPath.replace( '(instrumental)', '(Inst.)' )
	szPath = szPath.replace( '(Instrumental)', '(Inst.)' )
	szPath = szPath.replace( 'instrumental', 'Inst.' )
	szPath = szPath.replace( 'Instrumental', 'Inst.' )
	return szPath

def SaveUrlImage( url, size, path, read=False ):
	imagepath = os.path.join( path, 'cover.jpg' )
	print( 'imagepath', imagepath )
	urlretrieve( url, imagepath )
	image = Image.open( imagepath )
	image.thumbnail( ( size, size ) )
	image.save( imagepath )
	return imagepath

def GetImageThumbnail( path, size ):
	image = Image.open( path )
	image.thumbnail( ( size, size ) )
	image.save( 'cover-1024.jpg' )
	with open( 'cover-1024.jpg', 'rb' ) as f:
		return f.read()
	

def GetUrlImageData( url, size ):
	urlretrieve( url, 'cover.jpg' )
	image = Image.open( 'cover.jpg' )
	image.thumbnail( ( size, size ) )
	image.save( 'cover-1024.jpg' )
	with open( 'cover-1024.jpg', 'rb' ) as f:
		return f.read()

def StrWidth( string ):
    return sum( 1 + ( unicodedata.east_asian_width( c ) in "WF" ) for c in string )


def MtFormat( string, width, align='<', fill='!' ):
    count = ( width - sum( 1 + ( unicodedata.east_asian_width( c ) in "WF" ) for c in string ) )
    return {
        '>': lambda s: fill * count + s,
        '<': lambda s: s + fill * count,
        '^': lambda s: fill * (count / 2) + s + fill * (count / 2 + count % 2),
	}[align](string)

def MtHelp():
	print(
'''Usage: python Mxplorer.py <option>
\tfa <artist name>
\t\tfind artist.
\tal
\t\tshow artist list found by fa operation at last.
\tsa <index>
\t\tselect artist in artist list by index.
\tad
\t\tshow artist detail information which is selected.
\ttrl
\t\tshot current album track list.
\thelp
\t\tshow this text.
\texit
\t\texit Mxplorer.
''')
	return


if __name__ == '__main__':
	os.system( 'color 60' )
	print( 'Music-eXplorer' )

	mx = Mxplorer()
	mx.SetNotTaggedDirectory( os.path.join( 'D:\\', 'Storage', 'STREAM', '_NotTagged' ) )
	mx.SetServiceDirectory( os.path.join( 'U:\\', 'Storage', 'STREAM' ) )

	while True:
		cmd = input( 'MX > ' )

		if cmd.lower().startswith( 'fa ' ):#find artist
			keyword = cmd[ len( 'fa ' ): ].strip()
			mx.SetFoundArtistList( keyword )
			for artist in mx.artists:
				print( artist['artist_index'], artist['artist_name'] )

		elif cmd.lower() == 'al':#artist list
			if mx.artists == None:
				print( 'no artist list. use fa command.' )
			else:
				for artist in mx.artists:
					print( artist['artist_index'], artist['artist_name'] )

		elif cmd.lower().startswith( 'sa ' ):#select artist
			if mx.artists == None:
				print( 'no artist list. use fa command.' )
			else:
				try:
					index = int( cmd[ len( 'sa ' ): ].strip() )
				except:
					print( 'usage: sa <album index as integer>' )
					continue

				mx.SelectArtist( index )
				print( '%d ("%s") is selected.' % ( index, mx.artist['name'] ) )
		elif cmd.lower().startswith( 'sal ' ):#select album
			if not mx.albums:
				print( 'error. no selected album list. use sa command.')
				continue
			try:
				index = int( cmd[ len( 'sal ' ): ].strip() )
			except:
				print( 'usage: sal <album index as integer>' )
				continue

			mx.SelectAlbum( index )
			print( '%d ("%s") is selected.' % ( index, mx.album['album_name'] ) )
		elif cmd.lower() == 'sad':#show artist detail
			if not mx.artist:
				print( 'error. no selected artist info. use sa command.' )
				continue
			mx.ShowArtistInfo()
		elif cmd.lower() == 'all':#album list
			if not mx.artist:
				print( 'error. no selected artist info. use sa command.' )
				continue
			mx.ShowAlbumList()
		elif cmd.lower() == 'trl':#track list
			if not mx.album:
				print( 'error. no selected album info. use sal command.')
				continue
			mx.ShowTackList()
		elif cmd.lower() == 'mkdir':#make album directory
			if not mx.szServiceDir:
				print( 'error. threr is no a service directory path.' )
				continue
			if not mx.artist:
				print( 'error. no selected artist info. use sa command.' )
				continue
			if not mx.album:
				print( 'error. no selected album info. use sal command.')
				continue
			szDir = mx.GetAlbumDirectory()
			print( 'make album directory. "%s"' % szDir )
			if not os.path.exists( szDir ):
				os.makedirs( szDir )
		elif cmd.lower() == 'mkal':#Make album
			# not tagged 폴더에서 선택된 앨범의 트랙 이름과 비슷한 것들을 찾아서 맞는지 확인.
			print( 'make album.' )
		elif cmd.lower().startswith( 'str ' ):#select track
			if not mx.listTrack:
				print( 'error. no selected album. use sal command.')
				continue
			try:
				index = int( cmd[ len( 'str ' ): ].strip() )
			except:
				print( 'usage: sal <album index as integer>' )
				continue

			mx.SelectTrack( index )
			print( '%d (%s) is selected.' % ( index, mx.listTrack[index]['name'] ) )
		elif cmd.lower().startswith( 'sw '):#switch track
			# mkal 로 만든 리스트에서 두 트랙의 자리를 바꾼다.
			print( 'switch.' )
		elif cmd.lower().startswith( 'match '):#match
			if mx.nCurrentTrack is None:
				print( 'error. no selected track. use str command.')
				continue
			try:
				index = int( cmd[ len( 'match ' ): ].strip() )
			except:
				print( 'usage: sal <album index as integer>' )
				continue
			mx.AssignNotMatched( index )
		elif cmd.lower().startswith( 'skt ' ):#skip track
			if not mx.listTrack:
				print( 'error. no selected album. use sal command.')
				continue
			try:
				index = int( cmd[ len( 'skt ' ): ].strip() )
			except:
				print( 'usage: skt <album index as integer>' )
				continue

			mx.SkipTrack( index )
		elif cmd.lower().startswith( 'qm ' ):#quick match
			if not mx.listTrack:
				print( 'error. no selected album. use sal command.')
				continue
			list = cmd[ len( 'qm ' ): ].split( ' ' )
			try:
				index_track = int( list[0].strip() )
				index_match = int( list[1].strip() )
			except:
				print( 'usage: qm <track-index> <not-tagged-index>' )
				continue
			mx.SelectTrack( index_track )
			mx.AssignNotMatched( index_match )
		elif cmd.lower() == 'qma':#quick match album
			print( 'Quick Match Album' )
			mx.QuickMatchAlbum()
			mx.ShowCurrentTask()
		elif cmd.lower() == 'commit':#commit
			mx.Commit()
			print( 'commit.' )
		elif cmd.lower() == 'commit-image':#commit
			mx.CommitCover()
			print( 'commit cover.' )
		elif cmd.lower() == 'sct':#show current task
			mx.ShowCurrentTask()
		elif cmd.lower() == 'snt':#show not tagged
			mx.ShowNotTaggedList()
		elif cmd.lower() == 'help':#help
			MtHelp()
		elif cmd.lower() == 'exit':
			break
		elif cmd == '':
			continue
		else:
			print( 'Unknown command.' )
			MtHelp()

