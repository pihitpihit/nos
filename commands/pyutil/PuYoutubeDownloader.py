#-*- coding: utf-8 -*-

import sys
import os
import shutil
import youtube_dl


class Logger(object):
	def debug(self, msg):
		print( 'dbg', msg )
		pass

	def warning(self, msg):
		print( 'wrn', msg )
		pass

	def error(self, msg):
		print( 'err', msg )


class ydl:
	def __init__( self ):
		self.szLastFile = None
		self.dictOptions = {
			'outtmpl': '%(title)s.%(ext)s',
			'format': 'bestaudio/best',
			'postprocessors': [{
				'key': 'FFmpegExtractAudio',
				'preferredcodec': 'mp3',
				'preferredquality': '320',
			}],
			'logger': Logger(),
			'progress_hooks': [ self.OnEvent ],
			'cachedir': False,
		}
		self.hydl = youtube_dl.YoutubeDL( self.dictOptions )
		self.szArchiveDirectory = None
		self.szMp3File = None
		self.fnOnArchive = None
		self.hUserData = None
		self.nOnProgressCounter = 0
		self.nOnProgressInterval = 0
		return

	def OnEvent( self, event ):
		if event['status'] == 'downloading':
			self.OnProgress( event['_percent_str'] )
		elif event['status'] == 'finished':
			self.OnDownloaded( event['filename'] )
		else:
			print( '================' )
			print( event )
			print( '----------------' )

	def OnStart( self, szUrl ):
		self.szMp3File = None
		print( 'start download', szUrl, len( szUrl ), sys.getsizeof( szUrl ) )
		return

	def OnProgress( self, szPercent ):
		self.MoveLastFile()
		print( szPercent )
		if (self.fnOnProgress) and (self.nOnProgressCounter % self.nOnProgressInterval == 0):
			result = self.fnOnProgress( szPercent, self.hUserData )
			if result:
				self.hUserData = result
		self.nOnProgressCounter += 1
		return

	def OnDownloaded( self, szFilename ):
		self.szOrigFile = szFilename
		self.szMp3File = self.ConvertPathWebmToMp3( szFilename )
		print( 'download completed', self.szOrigFile )
		if self.fnOnDownloaded:
			result = self.fnOnDownloaded( self.szMp3File, self.hUserData )
			if result is not None:
				self.hUserData = result
		self.nOnProgressCounter = 0
		return

	def OnFinish( self, szFilename ):
		self.szMp3File = self.ConvertPathWebmToMp3( szFilename )
		self.MoveLastFile()
		print( 'overall completed', self.szMp3File )
		return

	def OnArchive( self ):
		if self.fnOnArchive:
			result = self.fnOnArchive( self.szMp3File, self.hUserData )
			if result is not None:
				self.hUserData = result
		return

	def SetOnProgress( self, fnCallback, nInterval=0 ):
		self.fnOnProgress = fnCallback
		self.nOnProgressInterval = nInterval
		return
	def SetOnDownloaded( self, fnCallback ):
		self.fnOnDownloaded  = fnCallback
		return
	def SetOnArchive( self, fnCallback ):
		self.fnOnArchive = fnCallback
		return

	def SetUserData( self, hUserData ):
		self.hUserData = hUserData
	def GetUserData( self ):
		return self.hUserData

	def Download( self, szUrl ):
		self.OnStart( szUrl )
		self.hydl.download( [szUrl] )
		self.OnFinish( self.szOrigFile )
		return

	def GetLastFile( self ):
		return self.szMp3File

	def MoveLastFile( self, szDir = None ):
		if self.szMp3File is None:
			return
		if szDir is None:
			if self.szArchiveDirectory is None:
				return
			szDir  = self.szArchiveDirectory
		print( 'move converted mp3 [%s]->[%s]' % ( self.szMp3File, szDir ) )
		shutil.move( self.szMp3File, os.path.join( szDir, self.szMp3File ) )
		self.OnArchive()
		self.szMp3File = None
		return

	def ConvertPathWebmToMp3( self, szWebm ):
		return os.path.splitext( szWebm )[0] + '.mp3'

	def SetArchiveDirectory( self, szDir ):
		if not os.path.exists( szDir ):
			raise
		self.szArchiveDirectory = szDir
		return


if __name__ == '__main__':
	h = ydl()
	h.SetArchiveDirectory( os.path.join( 'D:\\', 'Develop', 'commands', 'pyutil', '_NotTagged' ) )
	h.Download( sys.argv[1] )
	h.MoveLastFile( sys.argv[2] )

