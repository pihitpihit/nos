#-*- coding: utf-8 -*-

import os
import shutil
import pickle

from PuInstImport import InstImport
selenium = InstImport( 'selenium' )
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

szDefaultDriverDir  = os.path.join( '..', '..', 'tools', 'chromedriver_win32' )
szDefaultDriverName = 'chromedriver'
szDefaultDriverPath = os.path.join( szDefaultDriverDir, szDefaultDriverName + '.exe' )

class WebBrowser:
	def __init__( self, szServiceName, bSilent = True ):
		self.szServiceName = szServiceName
		( self.szDriverPath, self.szDriverName ) = self.MakeDriver( szServiceName )
		self.driver = None
		self.bSilent = bSilent
		self.bCookieLoaded = False
		return

	def MakeDriver( self, szServiceName ):
		szNewDriverName = '%s-%s.exe' % ( szDefaultDriverName, szServiceName )
		szNewDriverPath = os.path.join( szDefaultDriverDir, szNewDriverName )
		if os.path.isfile( szNewDriverPath ):
			pass
		else:
			shutil.copy( szDefaultDriverPath, szNewDriverPath )
		return ( szNewDriverPath, szNewDriverName )

	def Start( self ):
		os.system( 'TASKKILL /IM %s /T /F > NUL 2>&1' % self.szDriverName )
		self.driver = self.CreateDriverHandle()
		return

	def End( self ):
		self.driver.quit()
		return

	def SaveCookie( self ):
		c = self.driver.get_cookies()
		with open( self.GetCookiePath(), 'wb' ) as f:
			pickle.dump( c, f )
		return

	def LoadCookie( self ):
		try:
			f = open( self.GetCookiePath(), 'rb' )
			cookies = pickle.load( f )
			f.close()
			for c in cookies:
				self.driver.add_cookie( c )
			self.bCookieLoaded = True
		except:
			raise
			self.bCookieLoaded = False
		return self.bCookieLoaded

	def GetCookiePath( self ):
		szCookiePath = self.szDriverPath.replace( '.exe', '.cookie' )
		return szCookiePath

	def CreateDriverHandle( self ):
		if self.bSilent:
			options = webdriver.ChromeOptions()
			options.add_argument( 'headless' )
			options.add_argument( 'window-size=1920x1080' )
			options.add_argument( 'disable-gpu' )
			return webdriver.Chrome( self.szDriverPath, options=options )
		else:
			return webdriver.Chrome( self.szDriverPath )

	def Goto( self, szUrl ):
		self.driver.get( szUrl )
		self.Wait( 3 )
		return

	def GetPage( self ):
		return self.driver.page_source

	def Refresh( self ):
		self.driver.refresh()
		self.Wait( 3 )
		return

	def Wait( self, nSec ):
		self.driver.implicitly_wait( nSec )
		return

	def WaitForElement( self, nSec, szElement ):
		return WebDriverWait( self.driver, nSec ).until(
			EC.presence_of_element_located( ( By.CSS_SELECTOR, szElement ) )
		)

	def ExecuteScript( self, szScript ):
		self.driver.execute_script( szScript )
		return

	def GetElement( self, type, key ):
		element = None
		self.Wait( 1 )
		if type == 'name':
			element = self.driver.find_element_by_name( key )
		if type == 'xpath':
			element = self.driver.find_element_by_xpath( key )
		self.Wait( 1 )
		return element

	# 가끔 실패하는 경우가 있는데, 1초씩 대기하면서 최대 5회 재시도 한다.
	def GetAlert( self ):
		alert = None
		for i in range( 5 ):
			try:
				alert = self.driver.switch_to_alert()
				break
			except:
				self.hEvent.wait( 1 )
				continue
		return alert

	def GetElements( self, type, key ):
		elements = None
		self.Wait( 1 )
		if type == 'name':
			elements = self.driver.find_elements_by_name( key )
		if type == 'xpath':
			elements = self.driver.find_elements_by_xpath( key )
		self.Wait( 1 )
		return elements


if __name__ == '__main__':
	bro = WebBrowser( 'test' )
	bro.Start()
	print( '접속중.' )
	bro.Goto( 'http://webhills.iptime.org' )
	print( '접속중.(완료)' )
	print( '로그인중.' )
	bro.GetElement( 'name', 'login_id' ).send_keys( '2101202' )
	bro.GetElement( 'name', 'login_pw' ).send_keys( '4702' )
	bro.GetElement( 'xpath', '/html/body/div/div/form/center/button[1]' ).click()
	print( '로그인중.(완료)' )
	print( '차량검색중.' )
	targets = ['04노4662', '서울32더2989', '28러1111' ]
	for car in targets:
		bro.GetElement( 'name', 'searchCarNo' ).send_keys( car )
		bro.Wait( 3 )
		bro.GetElement( 'name', 'btnSearch' ).click()
		rows = bro.GetElements( 'xpath', '//*[@id="divAjaxCarList"]/tr/td/a' )
		print( '%s : %s' % ( car, len( rows ) != 0 ) )
	print( '차량검색중.(완료)' )
	print( 'Driver종료중' )
	bro.End()
	print( '(끝)' )
