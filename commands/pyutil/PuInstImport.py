#-*- coding: utf-8 -*-

import importlib
import pip

def PipVersion():
	print( pip.__version__ )

def InstImport( package, import_name = None ):
	if not import_name:
		import_name = package

	# step1. try load
	try:
		return importlib.import_module( import_name )
	except ImportError:
		pass

	# step2. try install
	try:
		pip._internal.main([ 'install', package ])
	except:
		raise 'Auto Install Failed..'

	module = importlib.import_module( import_name )
	globals()[package] = module
	return module

if __name__ == '__main__':
	PipVersion()
	InstImport( 'paramiko' )
