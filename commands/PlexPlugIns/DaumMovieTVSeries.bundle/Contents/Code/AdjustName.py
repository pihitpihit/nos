
def adjustName( name ):
    list = name.split( ',' )
    if 2 <= len( list ):
        left = ','.join( list[0:-1] )
        right = list[-1]
        try:
            year = int( right.strip().split( ' ' )[0].strip() )

            for i in range( len( left ) ):
                if left[i].isalpha() or left[i].isdigit():
                    return ( left[i:], year )
            return ( left, year )

        except:
            pass

        return ( list[0].strip(), -1 )

    else:
        return ( name, -1 )