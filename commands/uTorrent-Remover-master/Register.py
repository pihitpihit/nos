#-*- coding: utf-8 -*-

import requests
import sys
import os
import json
import easygui
import traceback
import urllib
from urllib.parse import urlencode

# set variables to connect to uTorrent web UI
port = 9091
username = 'tempo'
password = 'tempo!@#'

# set action urls
list_url = "http://localhost:{port}/gui/?list=1"
register_url = "http://localhost:{port}/gui/?action=add-url&{magnet}&download_dir={dir}"
list_dirs = "http://localhost:{port}/gui/?action=list-dirs"


def Register( magnet, storage ):

	szMagnet = urlencode( { 's' : magnet } )

	# Get dir list
	nDir = -1
	szDir = None

	try:
		szRequest = list_dirs.format( port=port )
		r = requests.get( szRequest, auth=( username, password ) )
		listDirs = r.json()['download-dirs']
		for i in range( 1, len( listDirs ) ):
			if storage in listDirs[i]['path']:
				nDir = i
				szDir = listDirs[i]['path']
				break

		# open URL to get data
		szRequest = register_url.format( port=port, magnet=szMagnet, dir=nDir )
		r = requests.get( szRequest, auth=( username, password ) )

		print( r.json() )

	except:
		traceback.print_exc()
		szDir = None

	return szDir


def Enum():
	szRequest = list_url.format( port=port )
	r = requests.get( szRequest, auth=( username, password ) )
	json_obj = json.loads( r.content )[u'torrents']
	return json_obj

if __name__ == "__main__":
	Register()

