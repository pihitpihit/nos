#-*- coding: utf-8 -*-

# import requests
import requests
import sys
import os
import json
import easygui
import traceback

sys.path.insert( 0, os.path.join( "D:\\", "Develop", "commands" ) )
import ScNotifySimple

# set variables to connect to uTorrent web UI
port = 9091
username = 'tempo'
password = 'tempo!@#'

# set action urls
list_url = "http://localhost:{port}/gui/?list=1".format(port=port)
remove_url = "http://localhost:{port}/gui/?action=remove&hash={match}"

def Clear():
	# open URL to get data
	r = requests.get(list_url, auth=(username, password))

	# return json object using 'torrents' index to narrow object to just torrents
	#json_obj = r.json()['torrents']
	json_obj = json.loads( r.content )[u'torrents']

	# for each torrent in the json object, check if it is finished and delete if so
	print( "=========================" )
	print( json_obj )
	print( "=========================" )
	for torrent in json_obj:
		ustr = torrent[2]
		print( ustr.encode( "utf-8" ).decode( "utf-8" ) )
		print( "-------------------------" )
		finished_parse = str(torrent).find("100.0 %")
		if finished_parse != -1:
			match = torrent[0].strip("'")
			if match:
				remove = requests.get(remove_url.format(port=port, match=match),
									  auth=(username, password))
				try:
					ScNotifySimple.Notify( sys.argv[1], ustr )
					#easygui.msgbox( sys.argv[1], 'success' )
				except Exception:
					easygui.msgbox( traceback.format_exc(), 'error' )
	return

if __name__ == "__main__":
	Clear()
